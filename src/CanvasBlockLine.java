// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Canvas1.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;                  //乱数や時間
//import java.io.*;                  //InputStream OutputStream
//import javax.microedition.rms.*;   //データセーブ用
//import javax.microedition.media.*; //音楽再生
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;      //通信
import MyPackage.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
public class CanvasBlockLine extends Canvas1 implements CommandListener {
	private final int         DISPLAY_WIDTH  = 240,
	                          DISPLAY_HEIGHT = 268;
	private final int         CLEAR=-8, SEND=-10, SOFT1=-6, SOFT2=-7, SOFT3=-20, SOFT4=-21;
	private final int         GRC_LT=Graphics.LEFT|Graphics.TOP,
	                          GRC_RT=Graphics.RIGHT|Graphics.TOP,
	                          GRC_HT=Graphics.HCENTER|Graphics.TOP;

	private final String      appver="ver 2.0";
//	private MIDlet            midlet;
	private Graphics          g;
	private Random            rndm=new Random();      //乱数
	private boolean           RunningFlag=true;
	private boolean           CallFlag=false;
	private int               keyEvent=-999;         //キーイベント
//	private int               keyState;              //キー状態
	private Command[]         commands=new Command[3];//コマンド
	private int               cmdnum=-1;
	private Image[]           images=new Image[1];
	
	private TiledLayer        tileBlock=null,
	                          tileNext =null,
	                          tileField=null;

	private Font              psFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL),
		                      pmFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM),
		                      plFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE);

	private MyPlayer          myplayer;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private final String[]    RS_HISCORE  ={"tm.blockline.hiscore1" ,
	                                        "tm.blockline.hiscore2" };
	private final String      RS_LASTSTAGE= "tm.blockline.laststage",
	                          RS_DESIGN   = "tm.blockline.design"   ,
	                          RS_LASTLINES= "tm.blockline.lastlines";
	
	private final int         BLOCK_W=14, BLOCK_H=14;
	
	private final int[][][]   BLOCKS={ { { 0, 0, 0, 0 },
	                                     { 1, 1, 1, 1 }
	                                   },
	                                   { { 2, 2, 0, 0 },
	                                     { 2, 2, 0, 0 }
	                                   },
	                                   { { 0, 3, 3, 0 },
	                                     { 3, 3, 0, 0 }
	                                   },
	                                   { { 4, 4, 0, 0 },
	                                     { 0, 4, 4, 0 }
	                                   },
	                                   { { 5, 0, 0, 0 },
	                                     { 5, 5, 5, 0 }
	                                   },
	                                   { { 0, 0, 6, 0 },
	                                     { 6, 6, 6, 0 }
	                                   },
	                                   { { 0, 7, 0, 0 },
	                                     { 7, 7, 7, 0 }
	                                   }};

	private final int         GS_DROP=10,
	                          GS_NEWBLOCK=11,
	                          GS_GAMEOVER=20,
	                          GS_GAMEOVER2=26,
	                          GS_FLASHLINE=12,
	                          GS_TITLE=0,
	                          GS_MESSAGE=1,
	                          GS_STAGECLEAR=21,
	                          GS_CONTINUE=22,
	                          GS_HISCORE=30,
	                          GS_ALLCLEAR=31,
	                          GS_SCORE1=23,
	                          GS_SCORE2=24,
	                          GS_SCORE3=25;
	private final int         PM_STAGE=0,
	                          PM_CHALLENGE=1;
	
	private int               GameState,
	                          PlayMode,
	                          messageCount,
	                          gameoverCount,
	                          nowDesign,
	                          designCount,
	                          rankin,
	                          acCount,
	                          volume,	                          
	                          volumeCount,
	                          deleteFlag;
	private int[][]           block=null,
	                          nextblock=null,
	                          hiscore=new int[2][5];
	private int               blockX, blockY,
	                          dropCount, dropSpeed, 
	                          flashCount,
	                          blockCount,
	                          score,
	                          drops,
	                          stage,
	                          lives,
	                          clearLine,
	                          blockListIndex;
	private int[]             flashLine=new int[5],
	                          lineCount=new int[5],
	                          blockList=new int[14],
	                          laststage=new int[5],
	                          lastlines=new int[5];
	private Hanabi[]          hanabi=new Hanabi[3];                        

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasBlockLine() {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		//midlet=m;
		//appver="var "+m.getAppProperty("MIDlet-Version");
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void initApp() {
		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		commands[0]=new Command("はい",  Command.OK,    0);
		commands[1]=new Command("いいえ",Command.CANCEL,0);
		commands[2]=new Command("終了",  Command.EXIT,  0);

		addCommand(commands[2]);

		//コマンドリスナーの指定
		setCommandListener(this);

		//イメージのロード等
		try {
			images[0]=Image.createImage("/blnTitle.png");

			Image img=Image.createImage("/blnBlock1.png");
			tileBlock=new TiledLayer(4,4,img,BLOCK_W, BLOCK_H);
			tileNext =new TiledLayer(4,4,img,BLOCK_W, BLOCK_H);
			tileField=new TiledLayer(12,19,img,BLOCK_W, BLOCK_H);
		} catch (Exception e) {}
		
		tileField.setPosition(0,0);
		
		String[] files={
			"/drmMove.mid",
			"/drmPut.mid",
			"/blnErase.mid",
			"/drmOver.mid",
			"/drmClear.mid",
			"/drmErase.mid"};
		myplayer=new MyPlayer(files,MyPlayer.TYPE_MIDI);
		myplayer.prefetchEx(0,0);
		myplayer.prefetchEx(1,1);
		volume=0;
		myplayer.setLevelAll(volume);
		volumeCount=0;
		
		for (int i=0;i<3;i++)
			hanabi[i]=new Hanabi();
	}

	private void initGame() {
		int i;
		
		blockListIndex=0;
		for (i=0;i<14;i++)
			blockList[i]=i%7;
		shuffle();
		
		resetField();
		nextblock=new int[4][4];
		block=nextblock;
		
		GameState=GS_TITLE;
		PlayMode=PM_STAGE;
		
		nowDesign=0;
		rankin=-1;
		
		loadData();
		changeDesign();
		
		deleteFlag=0;
	}

	//実行
	public void run() {
		int      i,j;
		long     sleepTime=0L;
		String   vol="";
		int      commentFlag=0; 

		initApp();
		initGame();
		
		while (RunningFlag) {
		
			if (commentFlag==5) {
				if (keyEvent!=-999) {
					commentFlag=0;
					addCommand(commands[2]);
					keyEvent=-999;
				}
			}
			
			//コマンドイベントの処理
			switch (cmdnum) {
			case 0:
				removeCommand(commands[0]);
				removeCommand(commands[1]);
				switch (commentFlag) {
				case 1:
					commentFlag=2;
					RunningFlag=false;
					break;
				case 4:
					commentFlag=5;
					deleteFlag=0;
					hiscore[PlayMode]=new int[5];
					if (PlayMode==PM_STAGE)
						laststage=new int[5];
					else
						lastlines=new int[5];
					break;
				}
				break;
			case 1:
				removeCommand(commands[0]);
				removeCommand(commands[1]);
				addCommand(commands[2]);
				commentFlag=0;
				if (deleteFlag==9)
					deleteFlag=0;
				break;
			case 2:
				removeCommand(commands[2]);
				addCommand(commands[0]);
				addCommand(commands[1]);
				commentFlag=1;
				break;
			}
			cmdnum=-999;
			
			switch (GameState) {
			case GS_TITLE:
				if ((deleteFlag==9)&&(commentFlag==0)) {
					removeCommand(commands[2]);
					addCommand(commands[0]);
					addCommand(commands[1]);
					commentFlag=4;
				}
				break;
			case GS_DROP:
			case GS_NEWBLOCK:
			case GS_FLASHLINE:
			case GS_MESSAGE:
				if (keyEvent==SEND) {
					if (commentFlag==0)
						commentFlag=3;
					else if (commentFlag==3)
						commentFlag=0;
				}
			}
			
			if (commentFlag==0) {
			
				//音のボリューム設定
				if (keyEvent==KEY_STAR) {
					volume+=20;
					if (volume>100) volume=0;
					myplayer.setLevelAll(volume);
					volumeCount=30;
					i=5-volume/20;
					vol="音量"+"■■■■■□□□□□".substring(i,i+5);
				}
				
				//メイン処理
				switch (GameState) {
				case GS_DROP:        gs_drop();        break;
				case GS_NEWBLOCK:    gs_newblock();    break;
				case GS_FLASHLINE:   gs_flashline();   break;
				case GS_TITLE:       gs_title();       break;
				case GS_MESSAGE:     gs_message();     break;
				case GS_STAGECLEAR:  gs_stageclear();  break;
				case GS_GAMEOVER:    
				case GS_GAMEOVER2:
				case GS_CONTINUE:    gs_gameover();    break;
				case GS_HISCORE:     gs_hiscore();     break;
				case GS_ALLCLEAR:    gs_allclear();    break;
				case GS_SCORE1:
				case GS_SCORE2:
				case GS_SCORE3:      gs_score();       break;
				}
				keyEvent=-999;
				
				
				//描画処理
				g.setColor(0,0,0);
				g.fillRect(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
	
				switch (GameState) {
				case GS_TITLE:
					drawTitle();
					break;
				case GS_HISCORE:
					drawHiscore();
					break;
				case GS_ALLCLEAR:
					drawAllclear();
					break;
				default:
					drawGame();
					break;
				}
				if (volumeCount>0) {
					g.setColor(0,255,0);
					g.setFont(psFont);
					g.drawString(vol,0,256,GRC_LT);
					volumeCount--;
				}
			
			} else {
				keyEvent=-999;
				switch (commentFlag) {
				case 1: drawComment("終了しますか？"); break;
				case 2: drawComment("終了しています"); break;
				case 3: drawComment("ＰＡＵＳＥ");     break;
				case 4: drawComment("ベストスコアを消去しますか？"); break;
				case 5: drawComment("消去しました"); break;
				}
			}
			flushGraphics();
            
			//スリープ
			while (System.currentTimeMillis()<sleepTime+100L);
			sleepTime=System.currentTimeMillis();
		}
		myplayer.close();
		
		if (CallFlag==false) saveData();

		//終了
		if (CallFlag==false) Project017.myproject.finishGame();
		//if (CallFlag==false) midlet.notifyDestroyed();

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		if (GameState==GS_TITLE) {
			switch (keyCode) {
			case KEY_NUM1:
				if (deleteFlag<3)
					deleteFlag++;
				if ((deleteFlag>5)&&(deleteFlag<9))
					deleteFlag++;
				break;
			case KEY_NUM3:
				if ((deleteFlag>2)&&(deleteFlag<6))
					deleteFlag++;
				break;
			}
		}
		
		switch (keyCode) {
		case KEY_NUM0:
			keyEvent=CLEAR;
			break;
		case CLEAR:
		case SEND:
		case KEY_STAR:
			keyEvent=keyCode;
			break;
		default:
			keyEvent=getGameAction(keyCode);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キーリピートイベント
	public void keyRepeated(int keyCode) {
		if (keyCode==0) return;
		int k=getGameAction(keyCode);
		switch (k) {
		case FIRE:
		case DOWN:
		case UP:
		case LEFT:
		case RIGHT:
			keyEvent=k;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		int i;
		for (i=0;i<commands.length;i++)
			if (c==commands[i]) {
				cmdnum=i;
				break;
			}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,128,255);
		g.drawRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,DISPLAY_WIDTH/2,DISPLAY_HEIGHT-18,Graphics.HCENTER|Graphics.TOP);
	} //*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//*	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	} //*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} //*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		RunningFlag=false;
		CallFlag=true;
		saveData();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void changeDesign() {
		try {
			Image img=Image.createImage("/blnBlock"+Integer.toString(nowDesign+1)+".png");
			tileBlock.setStaticTileSet(img,BLOCK_W,BLOCK_H);
			tileField.setStaticTileSet(img,BLOCK_W,BLOCK_H);
			tileNext.setStaticTileSet(img,BLOCK_W,BLOCK_H);
		} catch (Exception e) {}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//フィールドのリセット
	private void resetField() {
		tileField.fillCells(  1,  0, 11, 18, 0);
		//tileField.fillCells(  0,  0,  3,  1, 8);
		//tileField.fillCells(  9,  0,  3,  1, 8);
		tileField.fillCells(  0,  0,  1, 19, 8);
		tileField.fillCells( 11,  0,  1, 19, 8);
		tileField.fillCells(  0, 18, 12,  1, 8);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックを表示するTiledLayerにブロック情報を書き込む
	private void setBlock(TiledLayer tl, int[][] blk) {
		int i,j;
			for (j=0;j<4;j++)
				for (i=0;i<4;i++)
					tl.setCell(i,j,blk[i][j]);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//新しいブロックを生成し返す
	private int[][] getBlock(int type) {
		int     i,j;
		int[][] blk=new int[4][4];
		for (j=0;j<2;j++)
			for (i=0;i<4;i++)
				blk[i][j+2]=BLOCKS[type][j][i];
		return blk;
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックを左下側に寄せて返す
	private int[][] yoseruBlock(int[][] blk) {
		int i,j,c;
		do {
			c=0;
			for (i=0;i<4;i++)
				c+=blk[0][i];
			if (c==0)
				for (i=0;i<3;i++) 
					for (j=0;j<4;j++) {
						blk[i][j]=blk[i+1][j];
						blk[i+1][j]=0;
					}
		} while (c==0);
		do {
			c=0;
			for (i=0;i<4;i++)
				c+=blk[i][3];
			if (c==0)
				for (i=3;i>0;i--) 
					for (j=0;j<4;j++) {
						blk[j][i]=blk[j][i-1];
						blk[j][i-1]=0;
					}
		} while (c==0);
		return blk;
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//右回転させたブロックを返す
	private int[][] rotateBlockR(int[][] blk) {
		int     i,j;
		int[][] newblk=new int[4][4];
		
		for (j=0;j<4;j++)
			for (i=0;i<4;i++)
				newblk[i][j]=blk[j][3-i];
		return yoseruBlock(newblk);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//左回転させたブロックを返す
	private int[][] rotateBlockL(int[][] blk) {
		int     i,j;
		int[][] newblk=new int[4][4];
		
		for (j=0;j<4;j++)
			for (i=0;i<4;i++)
				newblk[i][j]=blk[3-j][i];
		return yoseruBlock(newblk);
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックの移動
	private void moveBlock(int x, int y) {
		blockX+=x;
		blockY+=y;
		tileBlock.move(x*BLOCK_W,y*BLOCK_H);
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックの右側の相対X位置を返す
	private int getRightSide(int[][] blk) {
		int i,j;
		for (j=3;j>=0;j--)
			for (i=3;i>=0;i--)
				if (blk[j][i]>0)
					return j;
		return 0;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックの上側の相対Y位置を返す
	private int getUpperSide(int[][] blk) {
		int i,j;
		for (j=0;j<3;j++)
			for (i=0;i<3;i++)
				if (blk[i][j]>0)
					return j;
		return 3;
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックが移動できるか判定
	private int movableBlock(int x, int y, int[][] blk) {
		int i,j;
		for (j=0;j<4;j++)
			for (i=0;i<4;i++) 
				if (blk[i][j]>0)
					if ((blockY+y+j>=0)&&(blockY+y+j<18)) {
						if ((blockX+x+i>=0)&&(blockX+x+i<12)) {
							if (tileField.getCell(blockX+x+i,blockY+y+j)>0)
								return 1;
						} else {
							return 1;
						}
					}
		for (j=0;j<4;j++)
			for (i=0;i<4;i++) 
				if (blk[i][j]>0)
					if ((blockY+y+j<0)||(blockY+y+j>=18))
						return -1;
		return 0;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ブロックリストのシャッフル
	private void shuffle() {
		int i,n1,n2,tmp;
		for (i=0;i<100;i++) {
			n1=i%14;
			n2=rand(14);
			tmp=blockList[n1];
			blockList[n1]=blockList[n2];
			blockList[n2]=tmp;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//新しいステージを生成
	private void makeStage() {
		int i,j;
		int[] tmp=new int[10];
		int   n=stage%10;
		resetField();
		for (j=0;j<n;j++) {
			for (i=0;i<10;i++)
				tmp[i]=rand(7)+1;
			for (i=0;i<7;i++)
				tmp[rand(10)]=0;
			for (i=0;i<10;i++)
				tileField.setCell(i+1,17-j,tmp[i]);
		}
		if (stage>9) {
			dropSpeed=5;
		} else {
			dropSpeed=15;
		}
		clearLine=25+5*(stage/100);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//新規スタート
	private void newGame() {
		int i;
		
		for (i=0;i<5;i++)
			lineCount[i]=0;
		resetField();
		dropSpeed=15;
		blockCount=0;
		score=0;
		drops=0;
		lives=2;
		rankin=-1;
		if (PlayMode==PM_STAGE) {
			stage=0;
			makeStage();
		}
		for (i=0;i<2;i++)
			gs_newblock();

		messageCount=0;
		GameState=GS_MESSAGE;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int checkHiscore(boolean rec) {
		int   i,r=-1;
		int[] hscore=hiscore[PlayMode];
		for (i=0;i<5;i++) {
			if (score>hscore[i]) {
				r=i;
				if (rec) {
					for (int j=4;j>i;j--) {
						hiscore[PlayMode][j]=hiscore[PlayMode][j-1];
						if (PlayMode==PM_STAGE)
							laststage[j]=laststage[j-1];
						else
							lastlines[j]=lastlines[j-1];
					}
					hiscore[PlayMode][i]=score;
					if (PlayMode==PM_STAGE)
						laststage[i]=stage+1;
					else
						lastlines[i]=lineCount[0];
				}
				break;
			}
		}
		return r;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//新しいブロックの処理
	private void gs_newblock() {
		int i,j;
		
		//次のブロックをセット
		block=nextblock;
		setBlock(tileBlock, block);
		blockX=4; blockY=-getUpperSide(block);
		tileBlock.setPosition(blockX*BLOCK_W,blockY*BLOCK_H);
		tileBlock.setVisible(true);

		//その次のブロックの読み出し
		nextblock=getBlock(blockList[blockListIndex]);
		int n=rand(4);
		for (i=0;i<n;i++)
			nextblock=rotateBlockR(nextblock);
		blockListIndex=(blockListIndex+1)%14;
		if (blockListIndex==0) shuffle();
		setBlock(tileNext, nextblock);
		tileNext.setPosition(182,28-getUpperSide(nextblock)*BLOCK_H);

		blockCount++;
		switch (PlayMode) {
		case PM_STAGE:
			if (dropSpeed>5) {
				if (blockCount%15==0) {
					dropSpeed--;
				}
			} else {
				if (dropSpeed>1) {
					if (blockCount%45==0) 
						dropSpeed--;
				}
			}
			break;
		case PM_CHALLENGE:
			int m=lineCount[0]/15;
			dropSpeed=15-m%14;
			break;
		}
		dropCount=dropSpeed;

		if (movableBlock(0,0,block)==0) {
			GameState=GS_DROP;
		} else {
			for (j=0;j<4;j++)
				for (i=0;i<4;i++)
					if (block[i][j]>0)
						block[i][j]=8;
			setBlock(tileBlock, block);
			myplayer.prefetchEx(2,3);
			myplayer.startEx(2);
			gameoverCount=0;
			if (PlayMode==PM_STAGE) {
				GameState=GS_CONTINUE;
			} else {
				GameState=GS_GAMEOVER;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//落下中の処理
	private void gs_drop() {
		int     i,j,flag,p1,p2;
		int[][] rtBlk;
		
		//キーイベントの処理
		switch (keyEvent) {
		case CLEAR:
			i=1;
			while (movableBlock(0,i,block)==0) i++;
			drops+=i-1;
			score+=i-1;
			moveBlock(0,i-1);
			dropCount=0;
			break;

		case FIRE: //ブロック右下中心回転
			rtBlk=rotateBlockR(block);
			p1=getRightSide(block)-getRightSide(rtBlk); //右詰め
			flag=movableBlock(p1,0,rtBlk);
			if (flag==0) {
				moveBlock(p1,0);
				block=rtBlk; setBlock(tileBlock, block);
			} else {
				if (flag>0) {
					for (i=1;i<=-p1;i++) {
						if (movableBlock(p1+i,0,rtBlk)==0) {
							moveBlock(p1+i,0);
							block=rtBlk; setBlock(tileBlock, block);
							flag=0;
							break;
						}
					}
				}
				if (flag!=0) 
					for (i=1;i<4;i++) {
						if (movableBlock(p1,i,rtBlk)==0) {
							moveBlock(p1,i);
							block=rtBlk; setBlock(tileBlock, block);
							break;
						}
					}
			}
			myplayer.startEx(0);
			break;

		case UP: //ブロック左下中心回転
			rtBlk=rotateBlockL(block);
			flag=movableBlock(0,0,rtBlk);
			if (flag==0) {
				block=rtBlk; setBlock(tileBlock, block);
			} else {
				if (flag>0) {
					p1=getRightSide(rtBlk)-getRightSide(block); //右詰め
					for (i=1;i<=p1;i++) {
						if (movableBlock(-i,0,rtBlk)==0) {
							moveBlock(-i,0);
							block=rtBlk; setBlock(tileBlock, block);
							flag=0;
							break;
						}
					}
				}
				if (flag!=0) 
					for (i=1;i<4;i++) {
						if (movableBlock(0,i,rtBlk)==0) {
							moveBlock(0,i);
							block=rtBlk; setBlock(tileBlock, block);
							break;
						}
					}
			}
			myplayer.startEx(0);
			break;

		default: 
			int keyState=getKeyStates();
			if ((keyState&LEFT_PRESSED)>0) { //左移動
				if (movableBlock(-1,0,block)==0) {
					moveBlock(-1,0);
				}
				myplayer.startEx(0);
			}
			if ((keyState&RIGHT_PRESSED)>0) { //右移動
				if (movableBlock(1,0,block)==0) {
					moveBlock(1,0);
				}
				myplayer.startEx(0);
			}
			if ((keyState&DOWN_PRESSED)>0) { //下移動
				dropCount=0;
				myplayer.startEx(0);
			}
			break;
		}
		
		//ブロックの落下の処理
		if (dropCount==0) { 
		
			if (movableBlock(0,1,block)==0) { //ブロックの落下
				moveBlock(0,1);
				dropCount=dropSpeed;

			} else { //ブロックの着地
				myplayer.startEx(1);
				//ブロックの配置
				for (j=0;j<4;j++)
					if (blockY+j>=0)
						for (i=0;i<4;i++)
							if (block[i][j]>0)
								tileField.setCell(blockX+i,blockY+j,block[i][j]);
				tileBlock.setVisible(false);
				//消えるラインがあるか判定
				int y,c,n=0;
				for (j=0;j<4;j++) {
					if ((y=blockY+j)>0) {
						c=0;
						for (i=1;i<=10;i++) {
							if (tileField.getCell(i,y)>0) 
								c++;
						}
						if (c==10) {
							n++;
							flashLine[n]=y;
						}
					}
				}
				if (n==0) {
					GameState=GS_NEWBLOCK;
				} else {
					flashCount=0; 
					flashLine[0]=n;
					myplayer.startEx(2);
					GameState=GS_FLASHLINE;
				}
			}
		}
		dropCount--;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gs_flashline() {
		
		if (flashCount%2==1)
			myplayer.startEx(2);
		
		if (flashCount==10) { //フラッシュさせる時間
			int i,j,k,y;
			int n=flashLine[0];

			for (k=1;k<=n;k++) { //ブロックを消す
				y=flashLine[k];
				for (j=y;j>1;j--) {
					for (i=1;i<=10;i++) {
						tileField.setCell(i,j,tileField.getCell(i,j-1));
					}
					tileField.fillCells(1,j-1,10,1,0);
				}
				if (k==1) {
					for (i=3;i<9;i++)
						tileField.setCell(i,1,tileField.getCell(i,0));
					tileField.fillCells(3,0,6,1,0);
				}
					
			}
			switch (n) {
			case 1: score+=  40; break;
			case 2: score+= 100; break;
			case 3: score+= 300; break;
			case 4: score+=1200; break;
			}
			lineCount[0]+=n;
			lineCount[n]++;
			if (PlayMode==PM_STAGE) {
				clearLine-=n;
				if (clearLine<1) {
					clearLine=0;
					myplayer.prefetchEx(2,5);
					myplayer.startEx(2);
					GameState=GS_STAGECLEAR;
					return;
				}
			}
			GameState=GS_NEWBLOCK;
		}
		flashCount++;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gs_title() {
		switch (keyEvent) {
		case SEND:  //ブロックのデザイン変更
			nowDesign=(nowDesign+1)%5;
			changeDesign();
			block=new int[4][4];
			for (int j=0;j<2;j++)
				for (int i=0;i<4;i++)
					block[i][j]=i+j*4+1;
			setBlock(tileBlock,block);
			tileBlock.setPosition(0,14);
			tileBlock.setVisible(true);
			designCount=30;
			break;
		case LEFT:
		case UP:
			PlayMode=PM_STAGE;
			myplayer.startEx(0);
			break;
		case RIGHT:
		case DOWN:
			PlayMode=PM_CHALLENGE;
			myplayer.startEx(0);
			break;
		case FIRE:
			myplayer.startEx(0);
			designCount=0;
			newGame();
			break;
		case CLEAR:
			rankin=-1;
			GameState=GS_HISCORE;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gs_message() {
		if (messageCount==20) {
			myplayer.prefetchEx(2,2);
			gs_newblock();
		}
		messageCount++;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void gs_gameover() {
		if (gameoverCount<3) {
			int i,j,y=gameoverCount*6;
			for (j=0;j<6;j++)
				for (i=1;i<=10;i++) {
					if (tileField.getCell(i,y+j)>0)
						tileField.setCell(i,y+j,8);
				}
			gameoverCount++;
		} else {
			if (keyEvent==FIRE) {
				resetField();
				tileBlock.setVisible(false);
				if (GameState==GS_GAMEOVER) {
					GameState=GS_SCORE1;
				}
				if (GameState==GS_CONTINUE) {
					GameState=GS_SCORE2;
				}
				if (GameState==GS_GAMEOVER2) {
					rankin=checkHiscore(true);
					if (rankin>=0) {
						myplayer.prefetchEx(2,4);
						myplayer.startEx(2);
					}
					GameState=GS_HISCORE;
				}
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void gs_stageclear() {
		if (keyEvent==FIRE) {
			resetField();
			tileBlock.setVisible(false);
			GameState=GS_SCORE3;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gs_hiscore() {
		if (keyEvent==FIRE) {
			GameState=GS_TITLE;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gs_allclear() {
		if (acCount==0)
			hanabi[0].makenew(DISPLAY_WIDTH/4,DISPLAY_HEIGHT/4,rand(5)+4);
		if (acCount==9)
			hanabi[1].makenew(DISPLAY_WIDTH/2,DISPLAY_HEIGHT/4,rand(5)+4);
		if (acCount==19)
			hanabi[2].makenew(DISPLAY_WIDTH*3/4,DISPLAY_HEIGHT/4,rand(5)+4);
		if (acCount<20) {
			acCount++;
		} else {
			if (keyEvent==FIRE) {
				stage++;
				score+=lives*10000;
				rankin=checkHiscore(true);
				if (rankin>=0) {
					//myplayer.prefetchEx(2,4);
					myplayer.startEx(2);
				}
				GameState=GS_HISCORE;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void gs_score() {
		if (keyEvent==FIRE) {
			switch (GameState) {
			case GS_SCORE1:
				rankin=checkHiscore(true);
				if (rankin>=0) {
					myplayer.prefetchEx(2,4);
					myplayer.startEx(2);
				}
				GameState=GS_HISCORE;
				break;
			case GS_SCORE2:
				for (int i=0;i<5;i++)
					lineCount[i]=0;
				drops=0;
				if (lives<1) {
					gameoverCount=10;
					GameState=GS_GAMEOVER2;
					return;
				}
				lives--;
				makeStage();
				messageCount=0;
				GameState=GS_MESSAGE;
				break;
			case GS_SCORE3:
				if (stage==19) {
					resetField();
					acCount=0;
					for (int i=0;i<hanabi.length;i++)
						hanabi[i].exist=false;
					myplayer.prefetchEx(2,4);
					myplayer.startEx(2);
					GameState=GS_ALLCLEAR;
					return;
				}
				for (int i=0;i<5;i++)
					lineCount[i]=0;
				drops=0;
				stage++;
				makeStage();
				messageCount=0;
				GameState=GS_MESSAGE;
				break;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawTitle() {
		int  x=DISPLAY_WIDTH/2;
		
		g.setFont(plFont);
		g.setColor(255,0,255);
		drawNukiMoji("ＢＬＯＣＫ　",x,55,255,0,255,GRC_HT);
		drawNukiMoji("　　ＬＩＮＥ",x,75,255,0,255,GRC_HT);
		
		g.drawImage(images[0],x,45,GRC_HT);
		
		g.setFont(psFont);
		g.setColor(255,255,255);
		g.drawString(appver,x,106,GRC_HT);
		
		g.setColor(255,255,0);
		g.drawString("Select Play Mode",x,145,GRC_HT);
		g.setFont(pmFont);
		if (PlayMode==PM_STAGE)
			g.setColor(0,255,255); else g.setColor(128,128,128);
		g.drawString("STAGE",x,170,GRC_HT);
		if (PlayMode==PM_CHALLENGE)
			g.setColor(0,255,255); else g.setColor(128,128,128);
		g.drawString("CHALLENGE",x,200,GRC_HT);

		if (designCount>0) {
			g.setFont(psFont);
			g.setColor(255,255,255);
			g.drawString("Change Design",0,0,GRC_LT);
			tileBlock.paint(g);
			designCount--;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawGame() {
		int i;
		
		g.setFont(psFont);
		g.setColor(255,255,255);
		g.drawString("NEXT",175,10,GRC_LT);
		
		g.drawString("SCORE",175,100,GRC_LT);
		int r=checkHiscore(false);
		if (r==0)
			g.setColor(0,255,255);
		if (r>0)
			g.setColor(255,255,0);
		g.setFont(pmFont);
		g.drawString(Integer.toString(score),235,115,GRC_RT);

		g.setColor(255,255,255);
		g.setFont(psFont);
		g.drawString("LINES",175,140,GRC_LT);
		
		g.setFont(pmFont);
		switch (PlayMode) {
		case PM_STAGE:
			g.drawString(Integer.toString(clearLine),235,155,GRC_RT);
			g.setFont(psFont);
			g.drawString("STAGE",175,180,GRC_LT);
			g.setFont(pmFont);
			g.drawString(Integer.toString(stage+1),235,195,GRC_RT);
			g.setFont(psFont);
			g.drawString("CONTINUE",175,220,GRC_LT);
			g.setFont(pmFont);
			g.drawString(Integer.toString(lives),235,235,GRC_RT);
			break;
		case PM_CHALLENGE:
			g.drawString(Integer.toString(lineCount[0]),235,155,GRC_RT);
			break;
		}

		tileField.paint(g);
		
		switch (GameState) {
		case GS_MESSAGE:
			g.setFont(pmFont);
			g.setColor(0,255,0);
			switch (PlayMode) {
			case PM_STAGE:
				drawNukiMoji("STAGE "+Integer.toString(stage+1),84,84,255,255,255,GRC_HT);
				break;
			case PM_CHALLENGE:
				drawNukiMoji("CHALLENGE MODE",84,84,255,255,255,GRC_HT);
				break;
			}
			g.setColor(0,255,0);
			drawNukiMoji("START!",84,115,255,255,255,GRC_HT);
			break;
		default:
			tileBlock.paint(g);
			tileNext.paint(g);
			break;
		}

		switch (GameState) {
		case GS_FLASHLINE:
			if (flashCount%2==0) {
				g.setColor(255,255,255);
				int y,n=flashLine[0];
				for (i=1;i<=n;i++) {
					y=flashLine[i]*BLOCK_H;
					g.fillRect(BLOCK_W,y,BLOCK_W*10,BLOCK_H);
				}
			}
			break;
		case GS_GAMEOVER:
		case GS_GAMEOVER2:
			if (gameoverCount>=2) {
				g.setFont(pmFont);
				g.setColor(255,255,255);
				drawNukiMoji("GAME OVER",84,115,255,0,0,GRC_HT);
			}
			break;
		case GS_STAGECLEAR:
			g.setFont(pmFont);
			g.setColor(255,0,255);
			drawNukiMoji("STAGE "+Integer.toString(stage+1),84,84,255,255,255,GRC_HT);
			
			g.setColor(255,0,255);
			drawNukiMoji("CLEAR!",84,115,255,255,255,GRC_HT);
			break;
		case GS_CONTINUE:
			g.setFont(pmFont);
			g.setColor(255,255,255);
			drawNukiMoji("FAILED!",84,115,255,0,0,GRC_HT);
			break;
		case GS_SCORE1:
		case GS_SCORE2:
		case GS_SCORE3:
			g.setFont(psFont);
			g.setColor(255,255,255);
			g.drawString("DROPS",28,28,GRC_LT);
			g.drawString(Integer.toString(drops),140,40,GRC_RT);
			g.drawString("1 LINE",28,60,GRC_LT);
			g.drawString(Integer.toString(lineCount[1])+" x   40 =",100,72,GRC_RT);
			g.drawString(Integer.toString(lineCount[1]*40),140,72,GRC_RT);
			g.drawString("2 LINES",28,90,GRC_LT);
			g.drawString(Integer.toString(lineCount[2])+" x  100 =",100,102,GRC_RT);
			g.drawString(Integer.toString(lineCount[2]*100),140,102,GRC_RT);
			g.drawString("3 LINES",28,120,GRC_LT);
			g.drawString(Integer.toString(lineCount[3])+" x  300 =",100,132,GRC_RT);
			g.drawString(Integer.toString(lineCount[3]*300),140,132,GRC_RT);
			g.drawString("4 LINES",28,150,GRC_LT);
			g.drawString(Integer.toString(lineCount[4])+" x 1200 =",100,162,GRC_RT);
			g.drawString(Integer.toString(lineCount[4]*1200),140,162,GRC_RT);
			int sc=lineCount[1]*40+lineCount[2]*100+lineCount[3]*300+lineCount[4]*1200+drops;
			g.drawString("SCORE",28,200,GRC_LT);
			g.drawString(Integer.toString(sc),140,212,GRC_RT);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawHiscore() {
		g.setFont(plFont);
		g.setColor(255,255,255);
		drawNukiMoji("BEST SCORE",120,10,0,0,0,GRC_HT);
		
		int y;
		int[] hscore=hiscore[PlayMode];

		g.setColor(255,255,255);
		g.setFont(pmFont);
		g.drawString("RANK",10,35,GRC_LT);
		g.drawString("SCORE",100,35,GRC_LT);
		if (PlayMode==PM_STAGE)
			g.drawString("STAGE",190,35,GRC_LT);
		else
			g.drawString("LINES",190,35,GRC_LT);
		g.setFont(plFont);
		for (int i=0;i<5;i++) {
			y=65+i*35;
			if (rankin==i)
				g.setColor(0,255,255);
			else
				g.setColor(255,255,255);
			g.drawString(Integer.toString(i+1),25,y,GRC_LT);
			g.drawString(Integer.toString(hscore[i]),170,y,GRC_RT);
			if (PlayMode==PM_STAGE) {
				if (laststage[i]>20)
					g.drawString("CL",230,y,GRC_RT);
				else
					g.drawString(Integer.toString(laststage[i]),230,y,GRC_RT);
			} else {
				g.drawString(Integer.toString(lastlines[i]),230,y,GRC_RT);
			}
		}
	}	

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawAllclear() {
		int i,c=0;
		
		g.setFont(plFont);
		g.setColor(255,0,255);
		drawNukiMoji("Congratulations!",DISPLAY_WIDTH/2,DISPLAY_HEIGHT/4,
		                                255,255,255,GRC_HT);
		
		g.setFont(pmFont);
		g.setColor(0,255,255);
		g.drawString("All Stage Clear !",DISPLAY_WIDTH/2,DISPLAY_HEIGHT/2,GRC_HT);
		
		g.setFont(psFont);
		g.setColor(255,255,255);
		g.drawString("Score "+Integer.toString(score),DISPLAY_WIDTH/2,DISPLAY_HEIGHT*3/4,GRC_HT);

		g.setColor(255,255,0);
		g.drawString("Continue Bonus",DISPLAY_WIDTH/2,DISPLAY_HEIGHT*3/4+30,GRC_HT);
		g.drawString("10000 x "+Integer.toString(lives)+" = "+Integer.toString(lives*10000),
		                               DISPLAY_WIDTH/2,DISPLAY_HEIGHT*3/4+45,GRC_HT);
		
		if (acCount>0) c++;
		if (acCount>9) c++;
		if (acCount>19) c++;
		for (i=0;i<c;i++) {
			hanabi[i].paint();
			hanabi[i].animate();
			if (hanabi[i].exist==false) {
				hanabi[i].makenew(DISPLAY_WIDTH*(i+1)/4, 
				                  DISPLAY_HEIGHT*(rand(4)+2)/10,
				                  rand(5)+4);
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private class Hanabi {
		public  boolean   exist=false;
		private int[]     x=new int[45],
		                  y=new int[45],
		                  vx=new int[45],
		                  vy=new int[45];
		private int       count=0;
		private int       color=0;

		public void makenew(int x0, int y0, int size) {
			int i,j;
			exist=true;
			count=30;
			int k=rand(6);
			if (k<3)
				color=255<<((k%3)*8);
			else
				color=0x00FFFFFF-(255<<((k%3)*8));
			for (i=0;i<45;i++) {
				x[i]=x0;
				y[i]=y0;
			}
			double d,v;
			for (j=0;j<45;j+=15) {
				v=(double)(size*(j/15+1));
				for (i=0;i<15;i++) {
					d=2.0*Math.PI*((double)i)/15.0;
					vx[i+j]=(int)(v*Math.cos(d));
					vy[i+j]=(int)(v*Math.sin(d))-21;
				}
			}
		}
		
		public void animate() {
			if (exist==false) return;
			
			for (int i=0;i<45;i++) {
				x[i]+=vx[i];
				y[i]+=vy[i];
				vy[i]+=7;
			}
			count--;
			if (count==0) exist=false;
		}
		
		public void paint() {
			if (exist==false) return;
			int i;
			int sz=count/10+1;
			int sz2=sz/2;
			g.setColor(color);
			for (i=0;i<45;i++) {
				g.fillArc(x[i]-sz2,y[i]-sz2,sz,sz,0,360);
			}
		}
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveData() {
		for (int i=0;i<2;i++)
			MyRecord.saveInts(RS_HISCORE[i],hiscore[i]);
		MyRecord.saveInts(RS_LASTSTAGE,laststage);
		MyRecord.saveInt(RS_DESIGN,nowDesign);
		MyRecord.saveInts(RS_LASTLINES,lastlines);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void loadData() {
		int[] data=null;
		
		for (int i=0;i<2;i++) {
			data=MyRecord.loadInts(RS_HISCORE[i]);
			if (data!=null) {
				hiscore[i]=data;
			}
		}
		data=MyRecord.loadInts(RS_LASTSTAGE);
		if (data!=null)
			laststage=data;
		nowDesign=MyRecord.loadInt(RS_DESIGN,0);
		data=MyRecord.loadInts(RS_LASTLINES);
		if (data!=null)
			lastlines=data;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Canvas1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
