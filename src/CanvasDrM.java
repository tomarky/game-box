// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// CanvasDrM.java メイン処理部
//
// (C)Tomarky   2009.09.11-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
import java.util.*;
//import javax.microedition.rms.*;   //データセーブ用
//import java.io.*;                  //バイト変換用
//import javax.microedition.media.*; //音楽再生
import MyPackage.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
class CanvasDrM extends Canvas1 implements CommandListener {
	private final String GAME_VERSION="1.2";
	private Graphics    g;
	private Image[]     imgs=new Image[2];
	private int         keyEvent=-999;         //キーイベント
	private int         keyState;              //キー状態
	private boolean     bl=true;
	private boolean     cf=false;
	private Command[]   cmds=new Command[7];//コマンド
	private int         cmd=-1;
	private Random      rndm=new Random();      //乱数

	private       int         w=240;
	private       int         h=268;
	//private       MIDlet      midlet;
	private       String      mdver;

	private final int         GM_TITLE=0,GM_SETTING=1,GM_START=2, 
	                          GM_NEWCAP=3,GM_MOVE=4,GM_CHKERASE=5,GM_ERASE=6,
	                          GM_GAMEOVER=7,GM_DROP=8,GM_CHKDROP=9,
	                          GM_GAMECLEAR=10,GM_HISCORE=11;
	private       int         GameMode=GM_TITLE;
	private       int         ivCount=0;

	private       int         iSelect=0;
	private       int         gmLevel=1;
	private       int         gmSpeed=1;
	private       int         iVirus=0;
	
	private final String[]    RC_NAME= {"tm.drmario.sound", 
	                                    "tm.drmario.hiscore1",
	                                    "tm.drmario.hiscore2",
	                                    "tm.drmario.hiscore3",
	                                    "tm.drmario.hiscore4",
	                                    "tm.drmario.hiscore5"};
	private       MyRecord    myrecord=new MyRecord();
	private       int[]       iHiscore=new int[5];
	private       int         iSelHiscore=-1;
	private       int         iScore=0;

	private final int   CP_RED=0,CP_BLUE=1,CP_YELLOW=2,
	                    CP_VIRUS=0,CP_BUBBLE=2,CP_ONE=3,
	                    CP_LEFT=4,CP_TOP=5,CP_RIGHT=6,CP_BOTTOM=7;
	private final int   FLD_NONE=-1,FLD_WALL=100;
	private int[][]     iFld=new int[10][22];
	private int[][]     ieFld=new int[10][22];

	private TiledLayer  tlFld;
	private Sprite      spCap;
	
	private int[]       iAnm=new int[3];
	
	private int[]       iNowCap=new int[2];
	private int[]       iNextCap=new int[2];
	private int         iCapX=0,iCapY=0,iCapR=0,iCapDp=0;
	private int         iDropSpeed=0;
	private int         iDrops=0;
	private int         iKillCount=0;
	private int         iRensa=0;
	
	private boolean     MesFlag=false;
	private boolean     MesFlag2=false;
	private boolean     MesFlag3=false;
	private int         imCount=0;
	private String      strMes="";

	private MyPlayer    myplayer;
	private int         useSnd=0;
	private int         iCurSnd=-1;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasDrM() {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		//midlet=m;
		//mdver=midlet.getAppProperty("MIDlet-Version");
		mdver=GAME_VERSION;
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void loadRecords() {
		int i;
		useSnd=myrecord.loadInt(RC_NAME[0],0);
		for (i=0;i<5;i++)
			iHiscore[i]=myrecord.loadInt(RC_NAME[i+1],0);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveRecords() {
		int i;
		myrecord.saveInt(RC_NAME[0],useSnd);
		for (i=0;i<5;i++)
			myrecord.saveInt(RC_NAME[i+1],iHiscore[i]);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void Init() {
		//グラフィックスの取得
		g=getGraphics();
		
		String[] se={"/drmMove.mid", 
	                 "/drmPut.mid",
	                 "/drmErase.mid",
	                 "/drmRensa2.mid",
	                 "/drmRensa3.mid",
	                 "/drmClear.mid",
	                 "/drmOver.mid",
	                 "/drmKill.mid"};
		
		myplayer=new MyPlayer(se,MyPlayer.TYPE_MIDI);
		//myplayer.prefetch(0); // move
		//myplayer.prefetch(1); // put
		myplayer.prefetchEx(2,1);

		loadRecords();
		
		if (useSnd==1) myplayer.setUse(false);

		//コマンドの生成
		cmds[0]=new Command("はい"  ,Command.OK,    0);
		cmds[1]=new Command("いいえ",Command.CANCEL,0);
		cmds[2]=new Command("ﾊｲｽｺｱ" ,Command.SCREEN,0);
		cmds[3]=new Command("ﾘｾｯﾄ"  ,Command.SCREEN,0);
		cmds[4]=new Command("終了"  ,Command.EXIT,  0);
		cmds[5]=new Command("やめる",Command.STOP,  0);
		cmds[6]=new Command("音"    ,Command.SCREEN,0);

		addCommand(cmds[4]);
		addCommand(cmds[2]);

		//コマンドリスナーの指定
		setCommandListener(this);

		try {
			imgs[0]=Image.createImage("/drmVirus.png");
			imgs[1]=Image.createImage("/drmBottle.png");
		} catch (Exception e){
			System.out.println(e.toString());
		}
		
		tlFld=new TiledLayer(10,22,imgs[0],8,8);
		spCap=new Sprite(imgs[0],8,8);
		
		iAnm[0]=tlFld.createAnimatedTile(1);
		iAnm[1]=tlFld.createAnimatedTile(9);
		iAnm[2]=tlFld.createAnimatedTile(17);
		
		tlFld.setPosition(80,46);
		tlFld.fillCells(0,0,10,22,0);

		ChangeGameMode(GM_TITLE);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//実行
	public void run() {
		long sleepTime=0L;
		int  i;

		Init();

		while (bl) {
		
			//コマンドイベントの処理
			switch (cmd){
			case 0: //はい
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				if (MesFlag) {
					bl=false;
				} else if (MesFlag2) {
					addCommand(cmds[4]);
					addCommand(cmds[3]);
					for (i=0;i<5;i++)
						iHiscore[i]=0;
					iSelHiscore=-1;
					MesFlag2=false;
				} else if (MesFlag3) {
					MesFlag3=false;
					ChangeGameMode(GM_GAMEOVER);
				}
				break;
			case 1: //いいえ
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				if (MesFlag3) {
					MesFlag3=false;
					addCommand(cmds[5]);
					addCommand(cmds[6]);
				} else {
					addCommand(cmds[4]);
					if ((GameMode==GM_SETTING)||(GameMode==GM_TITLE))
						addCommand(cmds[2]);
					if (GameMode==GM_HISCORE)
						addCommand(cmds[3]);
					MesFlag=false;
					MesFlag2=false;
				}
				break;
			case 2: //ハイスコア
				removeCommand(cmds[2]);
				ChangeGameMode(GM_HISCORE);
				break;
			case 3: //ハイスコアリセット
				MesFlag2=true;
				removeCommand(cmds[4]);
				removeCommand(cmds[3]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			case 4: //終了
				MesFlag=true;
				removeCommand(cmds[4]);
				removeCommand(cmds[2]);
				removeCommand(cmds[3]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			case 5: //中断
				MesFlag3=true;
				removeCommand(cmds[5]);
				removeCommand(cmds[6]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			case 6: //音 
				if (useSnd==0) {
					useSnd=1; myplayer.setUse(false);
					strMes="音:off"; imCount=15;
				} else {
					useSnd=0; myplayer.setUse(true);
					strMes="音:on "; imCount=15;
				}
				break;
			}
			cmd=-999;
			
			if (bl) {
			
				//描画
				if (MesFlag) {
					drawComment("終了しますか？");
				} else if (MesFlag2) {
					drawComment("ﾊｲｽｺｱをﾘｾｯﾄしますか？");
				} else if (MesFlag3) {
					drawComment("ゲームをやめますか？");
				} else {
					//画面クリア
					g.setColor(0,0,0);
					g.fillRect(0,0,w,h);
					
					switch (GameMode) {
					case GM_TITLE:     ml_title();     break;
					case GM_SETTING:   ml_setting();   break;
					case GM_START:     ml_start();     break;
					case GM_NEWCAP:    ml_newcap();    break;
					case GM_MOVE:      ml_move();      break;
					case GM_CHKERASE:  ml_chkerase();  break;
					case GM_ERASE:     ml_erase();     break;
					case GM_GAMEOVER:  ml_gameover();  break;
					case GM_DROP:      ml_drop();      break;
					case GM_CHKDROP:   ml_chkdrop();   break;
					case GM_GAMECLEAR: ml_gameclear(); break;
					case GM_HISCORE:   ml_hiscore();   break;
					}
					//簡易メッセージ
					if (imCount>0) {
						g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
						g.setColor(255,255,255);
						g.drawString(strMes,w-1,h-12-1,Graphics.RIGHT|Graphics.TOP);
						imCount--;
					}
				}
				flushGraphics();
	            
				//スリープ
				while (System.currentTimeMillis()<sleepTime+100);
				sleepTime=System.currentTimeMillis();
			}
		}

		//セーブ
		//saveRecords();

		//終了
		myplayer.close();
		
		if (cf==false) {
			saveRecords();
			Project017.myproject.finishGame();
		}
		//if (cf==false) midlet.notifyDestroyed();

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		keyEvent=getGameAction(keyCode);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		if (c==cmds[0]) cmd=0;
		if (c==cmds[1]) cmd=1;
		if (c==cmds[2]) cmd=2;
		if (c==cmds[3]) cmd=3;
		if (c==cmds[4]) cmd=4;
		if (c==cmds[5]) cmd=5;
		if (c==cmds[6]) cmd=6;
		repaint();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	private void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,h-20,w,20);
		g.setColor(0,128,255);
		g.drawRect(0,h-20,w,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,w/2,h-18,Graphics.HCENTER|Graphics.TOP);
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	}  

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		bl=false;
		cf=true;
		saveRecords();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@描写系処理@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int tlNum(int iCap) {
		int c=divColor(iCap);
		int t=divType(iCap);

		if ((iCap==FLD_NONE)||(iCap==FLD_WALL)) {
			return 0;
		}else if (t<2) {
			return iAnm[c];
		} else {
			return c*8+t+1;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int spNum(int iCap) {
		int c=divColor(iCap);
		int t=divType(iCap);
		
		if ((iCap==FLD_NONE)||(iCap==FLD_WALL)) {
			return -1;
		} else {
			return c*8+t;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int SubCapsulePX(int iRot) {
		switch (iRot) {
		case 0:   return  1;
		case 2:   return -1;
		default:  return  0;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int SubCapsulePY(int iRot) {
		switch (iRot) {
		case 1:   return  1;
		case 3:   return -1;
		default:  return  0;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void AnimeVirus() {
		if (tlFld.getAnimatedTile(iAnm[0])==1)
			tlFld.setAnimatedTile(iAnm[0],2);
		else
			tlFld.setAnimatedTile(iAnm[0],1);

		if (tlFld.getAnimatedTile(iAnm[1])==9)
			tlFld.setAnimatedTile(iAnm[1],10);
		else
			tlFld.setAnimatedTile(iAnm[1],9);

		if (tlFld.getAnimatedTile(iAnm[2])==17)
			tlFld.setAnimatedTile(iAnm[2],18);
		else
			tlFld.setAnimatedTile(iAnm[2],17);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void SetFld(int n) {
		int i,j,y1;
				
		y1=17-n*4;
		if (y1<0) y1=0;
		
		tlFld.fillCells(0,0,10,22,0);
		for (i=y1;i<22;i++)
			for (j=0;j<10;j++)
				tlFld.setCell(j,i,tlNum(iFld[j][i]));
	}
	private void SetFld() {
		SetFld(5);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawTitle() {
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
		g.setColor(255,255,255);
		drawNukiMoji("Like Dr.Mario",w/2,h/2-20,255,0,0,Graphics.HCENTER|Graphics.TOP);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(255,255,255);
		drawNukiMoji("ver."+mdver,w/2,h/2+5,255,0,0,Graphics.HCENTER|Graphics.TOP);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void stSelect(int n) {
		if (iSelect==n) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_UNDERLINED,Font.SIZE_MEDIUM));
			g.setColor(255,255,0);
		} else {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.setColor(255,255,255);
		}
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawSetting() {
		String[]  strSnd={"ON","OFF"};
		int x1=40;
		int x2=144;

		stSelect(0);
		g.drawString("VIRUS LEVEL",x1,h/2+40,Graphics.LEFT|Graphics.TOP);
		g.drawString(""+gmLevel   ,x2,h/2+40,Graphics.LEFT|Graphics.TOP);
		stSelect(1);
		g.drawString("SPEED"           ,x1,h/2+60,Graphics.LEFT|Graphics.TOP);
		g.drawString(SpeedName(gmSpeed),x2,h/2+60,Graphics.LEFT|Graphics.TOP);
		stSelect(2);
		g.drawString("SOUND"       ,x1,h/2+80,Graphics.LEFT|Graphics.TOP);
		g.drawString(strSnd[useSnd],x2,h/2+80,Graphics.LEFT|Graphics.TOP);
		stSelect(3);
		g.drawString("START",w/2,h/2+100,Graphics.HCENTER|Graphics.TOP);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawFld() {
		int i;
		
		AnimeVirus();
		g.drawImage(imgs[1],80,46,Graphics.LEFT|Graphics.TOP);
		tlFld.paint(g);
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(255,255,255);
		
		g.drawString("VIRUS",170,40,Graphics.LEFT|Graphics.TOP);
		g.drawString("LEVEL",182,52,Graphics.LEFT|Graphics.TOP);
		g.drawString(""+gmLevel,230,65,Graphics.RIGHT|Graphics.TOP);
		
		g.drawString("SPEED",170,88,Graphics.LEFT|Graphics.TOP);
		g.drawString(SpeedName(gmSpeed),230,101,Graphics.RIGHT|Graphics.TOP);
		
		g.drawString("VIRUS",170,124,Graphics.LEFT|Graphics.TOP);
		g.drawString(""+iVirus,230,137,Graphics.RIGHT|Graphics.TOP);

		g.drawString("TOP",10,40,Graphics.LEFT|Graphics.TOP);
		g.drawString(""+iHiscore[0],70,53,Graphics.RIGHT|Graphics.TOP);

		g.drawString("SCORE",10,76,Graphics.LEFT|Graphics.TOP);
		
		if (iHiscore[0]<iScore) {
			g.setColor(0,255,255);
		} else {
			for (i=1;i<5;i++) 
				if (iHiscore[i]<iScore) {
					g.setColor(255,255,0);
					break;
				}
		}
		g.drawString(""+iScore,70,89,Graphics.RIGHT|Graphics.TOP);

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawHiscore() {
		int i;
		
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));

		g.setColor(255,255,255);
		drawNukiMoji("HIGH-SCORE RANKING",w/2,10,0,0,0,Graphics.HCENTER|Graphics.TOP);

		for (i=0;i<5;i++) {
			if (i==iSelHiscore) 
				g.setColor(0,255,255);
			else
				g.setColor(255,255,255);
			g.drawString(""+(i+1)+" ...",60,30*i+40,Graphics.LEFT|Graphics.TOP);
			g.drawString(""+iHiscore[i],180,30*i+40,Graphics.RIGHT|Graphics.TOP);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawCap(int iCap, int x, int y) {
		int n=spNum(iCap);
		if (n<0) return;
		spCap.setPosition(x,y);
		spCap.setFrame(n);
		spCap.paint(g);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawNextCap() {
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(255,255,255);
		g.drawString("NEXT",80,10,Graphics.LEFT|Graphics.TOP);
		drawCap(iNextCap[0],112,12);
		drawCap(iNextCap[1],120,12);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawNowCap() {
		int x,y;
		
		x=iCapX*8+80;
		y=iCapY*8+46;
		drawCap(iNowCap[0],x,y);
		
		x+=8*SubCapsulePX(iCapR);
		y+=8*SubCapsulePY(iCapR);
		drawCap(iNowCap[1],x,y);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@ゲーム処理@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private String SpeedName(int n) {
		switch (n) {
		case 0:
			return "LOW";
		case 1:
			return "MEDIUM";
		case 2:
			return "HIGH";
		default:
			return "ERROR";
		}
	}
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void UpdateScore() {
		int i,s=1,d=0;
		if (iKillCount>0) {
			d=iKillCount-1;
			if (d>5) d=5;
			for (i=0;i<d;i++)
				s*=2;
			iScore+=s*100*(gmSpeed+1);
			iKillCount=0;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int getCapsule(int iColor, int iType) {
		return iColor*10+iType;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int divType(int iCap) {
		return iCap%10;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private int divColor(int iCap) {
		return iCap/10;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ResetFld() {
		int i,j;
		
		for (i=0;i<22;i++)
			for (j=0;j<10;j++)
				iFld[j][i]=FLD_NONE;
		
		for (i=4;i<22;i++) {
			iFld[0][i]=FLD_WALL;
			iFld[9][i]=FLD_WALL;
		}
		
		for (j=0;j<10;j++)
			iFld[j][21]=FLD_WALL;
		
		for (i=0;i<3;i++) {
			iFld[i][4]=FLD_WALL;
			iFld[9-i][4]=FLD_WALL;
		}
		
		for (i=3;i<7;i++) 
			iFld[i][0]=FLD_WALL;
		
		for (i=0;i<5;i++) {
			iFld[3][i]=FLD_WALL;
			iFld[6][i]=FLD_WALL;
		}
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void MakeStage() {
		int iVR,iVB,iVY,iVA;
		int rn;
		
		iVirus=gmLevel*4; if (iVirus>80) iVirus=80;
		iVR=iVB=iVY=iVirus/3;
		iVA=iVirus%3;
		if (iVA>0) {
			rn=rand(90);
			if (rn<30) {
				iVR++; if (iVA>1) iVB++;
			} else {
				rn=rand(100);
				if (rn<50) {
					iVB++; if (iVA>1) iVY++;
				} else {
					iVY++; if (iVA>1) iVR++;
				}
			}
		}
		
		int[][][] tmp=new int[3][4][6];
		int       i,j,k,x,y,v1;
		
		for (i=0;i<6;i++)
			for (j=0;j<4;j++)
				for (k=0;k<3;k++)
					tmp[k][j][i]=0;
		
		if (iVR>24) v1=24; else v1=iVR;
		for (i=0;i<v1;i++) { x=i%4; y=i/4; tmp[0][x][y]=1; }
		if (iVB>24) v1=24; else v1=iVB;
		for (i=0;i<v1;i++) { x=i%4; y=i/4; tmp[1][x][y]=1; }
		if (iVY>24) v1=24; else v1=iVY;
		for (i=0;i<v1;i++) { x=i%4; y=i/4; tmp[2][x][y]=1; }
		
		for (i=0;i<6;i++) 
			for (j=0;j<4;j++) 
				for (k=0;k<3;k++) {
					x=rand(4); y=rand(6);
					v1=tmp[k][j][i]; tmp[k][j][i]=tmp[k][x][y]; tmp[k][x][y]=v1;
				}

		for (i=0;i<6;i++) {
			y=i*2+8;
			for (j=0;j<4;j++) {
				x=j*2+1;
				if (tmp[0][j][i]>0) iFld[x][y]  =getCapsule(CP_RED,   CP_VIRUS);
				if (tmp[1][j][i]>0) iFld[x+1][y]=getCapsule(CP_BLUE,  CP_VIRUS);
				if (tmp[2][j][i]>0) iFld[x][y+1]=getCapsule(CP_YELLOW,CP_VIRUS);
				for (k=0;k<8;k++) {
					if (rand(100)<50) {
						v1=iFld[x][y]; iFld[x][y]=iFld[x+1][y];   iFld[x+1][y]=v1; }
					if (rand(100)<50) {
						v1=iFld[x][y]; iFld[x][y]=iFld[x][y+1];   iFld[x][y+1]=v1; }
					if (rand(100)<50) {
						v1=iFld[x][y]; iFld[x][y]=iFld[x+1][y+1]; iFld[x+1][y+1]=v1; }
				}
			}
		}
		
		if (iVirus<=68) {
			for (i=20;i>5;i--) 
				for (j=1;j<9;j++) {
					iFld[j][i]=iFld[j][i-1];
					iFld[j][i-1]=FLD_NONE;
				}
		} else {
			y=19-rand(9);
			for (i=20;i>y;i--) 
				for (j=1;j<9;j++) {
					iFld[j][i]=iFld[j][i-1];
					iFld[j][i-1]=FLD_NONE;
				}
			x=1;
			for (i=24;i<iVR;i++) { iFld[x][y]=getCapsule(CP_RED   ,CP_VIRUS); x++; }
			for (i=24;i<iVB;i++) { iFld[x][y]=getCapsule(CP_BLUE  ,CP_VIRUS); x++; }
			for (i=24;i<iVY;i++) { iFld[x][y]=getCapsule(CP_YELLOW,CP_VIRUS); x++; }
			for (k=0;k<10;k++) 
				for (j=1;j<9;j++) {
					x=rand(8)+1;
					v1=iFld[j][y];
					iFld[j][y]=iFld[x][y];
					iFld[x][y]=v1;
				}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ResetPersonal() {
		iScore=0;
		iSelHiscore=-1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ChkHiscore() {
		int i,fg=-1;
		
		for (i=0;i<5;i++) {
			if (iScore>iHiscore[i]) {
				fg=i;
				break;
			}
		}
		
		if (fg<0) return;
		
		for (i=4;i>fg;i--) {
			iHiscore[i]=iHiscore[i-1];
		}
		iHiscore[fg]=iScore;
		iSelHiscore=fg;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void NewGame() {
		ResetFld();
		SetFld();
		MakeStage();
		
		ml_newcap(); ml_newcap();
		
		iDropSpeed=14-3*gmSpeed;
		iDrops=0;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean ChkGameOver() {
		int px=SubCapsulePX(iCapR);
		int py=SubCapsulePY(iCapR);
		if (iFld[iCapX][iCapY]!=FLD_NONE) return true;
		if (iFld[iCapX+px][iCapY+py]!=FLD_NONE) return true;
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean ChkCapMove(int px, int py) {
		int x,y;
		
		x=iCapX+px;
		y=iCapY+py;
		
		if (iFld[x][y]==FLD_NONE) {
			x+=SubCapsulePX(iCapR);
			y+=SubCapsulePY(iCapR);
			if (iFld[x][y]==FLD_NONE) 
				return true;
		}
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void MoveCap(int px, int py) {
		iCapX+=px;
		iCapY+=py;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void TryCapMove(int px, int py) {
		if (ChkCapMove(px,py)) {
			MoveCap(px,py);
			//myplayer.start(0); //move
			myplayer.startEx(2);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void PutCap() {
		int x,y;
		
		iFld[iCapX][iCapY]=iNowCap[0];
		x=iCapX+SubCapsulePX(iCapR);
		y=iCapY+SubCapsulePY(iCapR);
		iFld[x][y]=iNowCap[1];
		SetFld();
	}
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private int RotTurn(int iRot, int n) {
		if (n==0) return (iRot+1)%4;
		else      return (iRot+3)%4;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean ChkCapTurn(int iRot, int n) {
		int rt=RotTurn(iRot,n);
		int px=SubCapsulePX(rt);
		int py=SubCapsulePY(rt);
		
		if (iFld[iCapX+px][iCapY+py]==FLD_NONE) return true;
		if (iFld[iCapX-px][iCapY-py]==FLD_NONE) return true;
		
		return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void TurnCap(int n) {
		int c1,c2,px,py;
		iCapR=RotTurn(iCapR,n);
		
		px=SubCapsulePX(iCapR);
		py=SubCapsulePY(iCapR);
		if (iFld[iCapX+px][iCapY+py]!=FLD_NONE) {
			MoveCap(-px,-py);
		}
		
		c1=divColor(iNowCap[0]);
		c2=divColor(iNowCap[1]);
		switch (iCapR) {
		case 0:
			iNowCap[0]=getCapsule(c1,CP_LEFT);
			iNowCap[1]=getCapsule(c2,CP_RIGHT);
			break;
		case 1:
			iNowCap[0]=getCapsule(c1,CP_TOP);
			iNowCap[1]=getCapsule(c2,CP_BOTTOM);
			break;
		case 2:
			iNowCap[0]=getCapsule(c1,CP_RIGHT);
			iNowCap[1]=getCapsule(c2,CP_LEFT);
			break;
		case 3:
			iNowCap[0]=getCapsule(c1,CP_BOTTOM);
			iNowCap[1]=getCapsule(c2,CP_TOP);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void TryCapTurn(int n) {
		if (ChkCapTurn(iCapR,n)) {
			TurnCap(n);
			//myplayer.start(0); //move
			myplayer.startEx(2);
		} 
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean ChkErase() {
		int i,j,k;
		int cl,tp,fg=0;
		int icl,icc,ist;
		
		for (i=0;i<22;i++)
			for (j=0;j<10;j++)
				ieFld[j][i]=0;

		for (i=0;i<22;i++) {
			icl=-1; icc=0; ist=0;
			for (j=0;j<10;j++) {
				if ((iFld[j][i]!=FLD_NONE)&&(iFld[j][i]!=FLD_WALL)) {
					cl=divColor(iFld[j][i]);
					tp=divType(iFld[j][i]);
					if (cl!=icl) {
						if ((icl>=0)&&(icc>3)) {
							for (k=ist;k<j;k++)
								ieFld[k][i]=1;
							fg++;
						}
						icl=cl; icc=1; ist=j;
					} else {
						icc++;
					}
				} else {
					if ((icl>=0)&&(icc>3)) {
						for (k=ist;k<j;k++)
							ieFld[k][i]=1;
						fg=1;
					}
					icl=-1; icc=0; ist=0;
				}
			}
		}
		
		for (j=0;j<10;j++) {
			icl=-1; icc=0; ist=0;
			for  (i=0;i<22;i++) {
				if ((iFld[j][i]!=FLD_NONE)&&(iFld[j][i]!=FLD_WALL)) {
					cl=divColor(iFld[j][i]);
					tp=divType(iFld[j][i]);
					if (cl!=icl) {
						if ((icl>=0)&&(icc>3)) {
							for (k=ist;k<i;k++)
								ieFld[j][k]=1;
							fg++;
						}
						icl=cl; icc=1; ist=i;
					} else {
						icc++;
					}
				} else {
					if ((icl>=0)&&(icc>3)) {
						for (k=ist;k<i;k++)
							ieFld[j][k]=1;
						fg=1;
					}
					icl=-1; icc=0; ist=0;
				}
			}
		}

		if (fg>0) return true;
		else      return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private int getRot(int iTp) {
		switch (iTp) {
		case CP_LEFT:
		case CP_TOP:
		case CP_RIGHT:
		case CP_BOTTOM:
			return iTp-CP_LEFT;
		default:
			return -1;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void DoErase(int n) {
		int i,j,cl,tp,rt,x,y,cl2;
		int vr=0;
		
		for (i=0;i<22;i++)
			for (j=0;j<10;j++) {
				if (ieFld[j][i]>0) {
					switch (n) {
					case 0:
						cl=divColor(iFld[j][i]);
						tp=divType(iFld[j][i]);
						if (tp==CP_VIRUS) {
							iKillCount++;
							iVirus--;
							vr++;
						}
						rt=getRot(tp);
						if (rt>=0) {
							x=j+SubCapsulePX(rt);
							y=i+SubCapsulePY(rt);
							cl2=divColor(iFld[x][y]);
							iFld[x][y]=getCapsule(cl2,CP_ONE);
						}
						iFld[j][i]=getCapsule(cl,CP_BUBBLE);
						break;
					case 1:
						iFld[j][i]=FLD_NONE;
						break;
					}
				}
			}
		SetFld();
		
		if (n==0) {
			if (vr>0) {
				myplayer.prefetchEx(1,7);
			} else {
				myplayer.prefetchEx(1,2);
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean ChkDrop() {
		int i,j,tp,fg=0;
		
		for (i=0;i<22;i++)
			for (j=0;j<10;j++) 
				ieFld[j][i]=0;
		
		for (i=20;i>0;i--) {
			for (j=0;j<10;j++) {
				if ((iFld[j][i]!=FLD_NONE)&&(iFld[j][i]!=FLD_WALL)) {
					if ((ieFld[j][i+1]==2)||(iFld[j][i+1]==FLD_NONE)) {
						tp=divType(iFld[j][i]);
						switch (tp) {
						case CP_ONE:
						case CP_TOP:
						case CP_BOTTOM:
							ieFld[j][i]=2;
							fg++;
							break;
						case CP_LEFT:
							ieFld[j][i]=1;
							ieFld[j+1][i]=1;
							break;
						case CP_RIGHT:
							ieFld[j-1][i]++;
							ieFld[j][i]++;
							if (ieFld[j][i]==2) fg++;
							break;
						}
					}
				}
			}
		}
		
		if (fg>0) return true;
		else      return false;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void DoDrop() {
		int i,j;
		
		for (i=20;i>0;i--) {
			for (j=0;j<10;j++) {
				if (ieFld[j][i]==2) {
					iFld[j][i+1]=iFld[j][i];
					iFld[j][i]=FLD_NONE;
				}
			}
		}
		SetFld();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@メインループ系処理@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ChangeGameMode(int igamemode) {
		switch (igamemode) {
		case GM_TITLE:
			ivCount=0;
			addCommand(cmds[2]);
			break;
		case GM_SETTING:
			iSelect=0;
			break;
		case GM_START:
			NewGame();
			ivCount=0;
			break;
		case GM_ERASE:
			ivCount=0;
			break;
		case GM_DROP:
			myplayer.prefetchEx(2,1);
			break;
		case GM_GAMEOVER:
			removeCommand(cmds[5]);
			removeCommand(cmds[6]);
			break;
		case GM_GAMECLEAR:
			ivCount=0;
			break;
		case GM_HISCORE:
			ivCount=0;
			addCommand(cmds[3]);
			//myplayer.start(1); //put
			myplayer.prefetchEx(2,1); myplayer.startEx(2);
			break;
		}
		GameMode=igamemode;		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_title() {
	
		if (keyEvent==FIRE) {
			//myplayer.start(0);
			myplayer.startEx(2);
			ChangeGameMode(GM_SETTING);
		}
		keyEvent=-999;
		
		drawTitle();
		
		if (ivCount==0) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.setColor(255,255,0);
			g.drawString("Press（●）Key",w/2,h/2+40,Graphics.HCENTER|Graphics.TOP);
		}
		ivCount=1-ivCount;

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_setting() {


		switch (keyEvent) {
		case UP: //Up
			if (iSelect>0) iSelect--; myplayer.startEx(2); //myplayer.start(1);
			break;
		case DOWN: //Down
			if (iSelect<3) iSelect++; myplayer.startEx(2); //myplayer.start(1);
			break;
		case LEFT: //Left
			switch (iSelect) {
			case 0:
				if (gmLevel>1) gmLevel--; myplayer.startEx(2); //myplayer.start(0);
				break;
			case 1:
				if (gmSpeed>0) gmSpeed--; myplayer.startEx(2); //myplayer.start(0);
				break;
			case 2:
				useSnd=0;
				myplayer.setUse(true); myplayer.startEx(2); //myplayer.start(0);
				break;
			}
			break;
		case RIGHT: //Right
			switch (iSelect) {
			case 0:
				if (gmLevel<20) gmLevel++; myplayer.startEx(2); //myplayer.start(0);
				break;
			case 1:
				if (gmSpeed<2) gmSpeed++; myplayer.startEx(2); //myplayer.start(0);
				break;
			case 2:
				useSnd=1;
				myplayer.setUse(false);
			}
			break;
		case FIRE: //FIRE
			if (iSelect==3) {
				ResetPersonal();
				removeCommand(cmds[2]);
				removeCommand(cmds[4]);
				addCommand(cmds[5]);
				addCommand(cmds[6]);
				ChangeGameMode(GM_START); myplayer.startEx(2); //myplayer.start(0);
			}
			break;
		}
		keyEvent=-999;
		
		drawTitle();
		drawSetting();
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_start() {
		
		drawFld();
		if (ivCount<7) {
			SetFld(ivCount/2);
		} else {
			ChangeGameMode(GM_NEWCAP);
		}
		ivCount++;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_newcap() {
		int i;
		
		iKillCount=0;
		iRensa=0;
		
		iDrops++;
		if ((iDrops%10)==0) {
			if (iDropSpeed>3) iDropSpeed--;
		}
		
		iCapX=4;
		iCapY=1;
		iCapR=0;
		iCapDp=iDropSpeed;
		
		for (i=0;i<2;i++)
			iNowCap[i]=iNextCap[i];
			
		iNextCap[0]=getCapsule(rand(3),CP_LEFT);
		iNextCap[1]=getCapsule(rand(3),CP_RIGHT);
		
		if (GameMode!=GM_NEWCAP) return;
		
		drawFld();
		drawNextCap();
		drawNowCap();
		
		myplayer.prefetchEx(2,0);
		myplayer.prefetchEx(1,1);
		
		ChangeGameMode(GM_MOVE);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_move() {
	
		drawFld();
		drawNextCap();
		
		if (ChkGameOver()) {
			keyEvent=-999;
			iCapDp=100;
			myplayer.prefetchEx(0,6); myplayer.startEx(0);
			ChangeGameMode(GM_GAMEOVER);
		}
		
		//Move
		switch (keyEvent) {
		case UP: //Up
			TryCapTurn(1);
			break;
		case FIRE: //FIRE
			TryCapTurn(0);
			break;
		default:
			keyState=getKeyStates();
			if ((keyState&LEFT_PRESSED)!=0) {
				TryCapMove(-1,0);
			} else if ((keyState&RIGHT_PRESSED)!=0) {
				TryCapMove(1,0);
			} else if ((keyState&DOWN_PRESSED)!=0) {
				iCapDp=0;
				myplayer.startEx(2); //myplayer.start(0);
			}
			break;
		}
		keyEvent=-999;

		//Drop
		if (iCapDp==0) {
			if (ChkCapMove(0,1)) {
				MoveCap(0,1);
				iCapDp=iDropSpeed;
			} else {
				PutCap();
				//myplayer.start(1);
				myplayer.startEx(1);
				ChangeGameMode(GM_CHKERASE);
			}
		} else {
			iCapDp--;
		}
		
		drawNowCap();
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_chkerase() {
		drawFld();
		drawNextCap();
		
		if (ChkErase()) {
			iRensa++;
			ChangeGameMode(GM_ERASE);
		} else {
			UpdateScore();
			if (iRensa==2) {
				myplayer.prefetchEx(0,3); myplayer.startEx(0);
			} else if (iRensa>2) {
				myplayer.prefetchEx(0,4); myplayer.startEx(0);
			}
			ChangeGameMode(GM_NEWCAP);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_erase() {
		switch (ivCount) {
		case 0:
			DoErase(0);
			myplayer.startEx(1);
			break;
		case 2:
			DoErase(1);
			break;
		case 3:
			if (iVirus==0) {
				UpdateScore();
				myplayer.prefetchEx(0,5); myplayer.startEx(0);
				ChangeGameMode(GM_GAMECLEAR);
			} else {
				ChangeGameMode(GM_CHKDROP);
			}
			break;
		}
		drawFld();
		drawNextCap();
		ivCount++;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_chkdrop() {
		drawFld();
		drawNextCap();
		if (ChkDrop()) {
			ChangeGameMode(GM_DROP);
		} else {
			ChangeGameMode(GM_CHKERASE);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_drop() {
		DoDrop();
		//myplayer.start(1);
		myplayer.startEx(2);
		ChangeGameMode(GM_CHKDROP);
		drawFld();
		drawNextCap();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_gameover() {
		drawFld();
		drawNextCap();
		drawNowCap();
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.setColor(255,0,0);
		g.drawString("Game Over",120,225,Graphics.HCENTER|Graphics.TOP);

		if (keyEvent==FIRE) {
			ChkHiscore();
			addCommand(cmds[4]);
			if (iSelHiscore>=0) {
				myplayer.prefetchEx(0,5); myplayer.startEx(0);
			}
			ChangeGameMode(GM_HISCORE);
		}
		keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_gameclear() {
		
	
		drawFld();
		drawNextCap();
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		if (ivCount==0)
			g.setColor(0,255,255);
		else
			g.setColor(255,255,255);
		g.drawString("Clear !",120,225,Graphics.HCENTER|Graphics.TOP);
		//g.setColor(0,255,255);
		g.drawString("Next Level "+(gmLevel+1),120,245,Graphics.HCENTER|Graphics.TOP);
		
		ivCount=1-ivCount;
		
		if (keyEvent==FIRE) {
			gmLevel++;
			ChangeGameMode(GM_START);
		}
		keyEvent=-999;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void ml_hiscore() {
		if (keyEvent==FIRE) {
			removeCommand(cmds[3]);
			//myplayer.start(1);
			myplayer.startEx(2);
			ChangeGameMode(GM_TITLE);
		}
		keyEvent=-999;
		
		drawHiscore();
		if (ivCount==0) {
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.setColor(255,255,255);
			g.drawString("Press（●）Key",w/2,h-50,Graphics.HCENTER|Graphics.TOP);
		}
		ivCount=1-ivCount;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //CanvasDrMの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
