// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Canvas1.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;                  //乱数や時間
//import java.io.*;                  //InputStream OutputStream
//import javax.microedition.rms.*;   //データセーブ用
//import javax.microedition.media.*; //音楽再生
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;      //通信
import MyPackage.*;


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
public class CanvasSisen extends Canvas1 implements CommandListener {
	private final int         DISPLAY_WIDTH  = 240,
	                          DISPLAY_HEIGHT = 268;
	private final int         CLEAR=-8, SEND=-10, SOFT1=-6, SOFT2=-7, SOFT3=-20, SOFT4=-21;
//	private MIDlet            midlet;
	private final String      appver="ver 1.2";
	private Graphics          g;
	private Random            rndm=new Random();      //乱数
	private boolean           RunningFlag=true;
	private boolean           CallFlag=false;
	private int               keyEvent=-999;         //キーイベント
	private int               keyState;              //キー状態
	private Command[]         commands=new Command[9];//コマンド
	private int               cmdnum=-1;
	private Image[]           images=new Image[1];

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private final String      RS_SCORE="tm.sisen.score",
	                          RS_DATE="tm.sisen.date";
	
	private TiledLayer        tileField=null;
	private int               curX,curY,paiCount,timeCount,clearFlag,
	                          selX,selY,select,logIndex,rankin,root,
	                          tfTop,tfLeft;
	private int[][]           lineX=new int[4][2],
	                          lineY=new int[4][2],
	                          field=new int[17][8],
	                          paiLog=new int[68][4];
	private boolean           recording,onetry;
	private int[]             hiscore=new int[10];
	private long[]            hiscoreDate=new long[10];


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasSisen() {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		//midlet=m;
		//appver="ver "+midlet.getAppProperty("MIDlet-Version");
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void initApp() {
		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		commands[0]=new Command("はい",  Command.OK,    0);
		commands[1]=new Command("いいえ",Command.CANCEL,0);
		commands[2]=new Command("終了",  Command.SCREEN,0);
		commands[3]=new Command("新規",  Command.SCREEN,1);
		commands[4]=new Command("サーチ",Command.SCREEN,2);
		commands[5]=new Command("戻す",  Command.SCREEN,3);
		commands[6]=new Command("再挑戦",Command.SCREEN,4);
		commands[7]=new Command("記録",  Command.SCREEN,5);
		commands[8]=new Command("戻る",  Command.SCREEN,1);

		addCommand(commands[2]);
		addCommand(commands[3]);
		addCommand(commands[4]);
		addCommand(commands[5]);
		addCommand(commands[6]);
		addCommand(commands[7]);
		

		//コマンドリスナーの指定
		setCommandListener(this);
		
		try {
			images[0]=Image.createImage("/sisenTitle.png");
			tileField=new TiledLayer(19,10,Image.createImage("/sisenPai.png"),12,15);
		} catch (Exception e) {}
		
		tfTop=60; tfLeft=6;
		tileField.setPosition(tfLeft,tfTop);
		tileField.fillCells(1,1,17,8,35);
	}
	
	public void initGame() {
		for (int i=0;i<10;i++)
			hiscore[i]=107999;
		
		loadData();
		
		clearFlag=0;
		root=0;
		
		curX=1;
		curY=1;

		newgame();
		onetry=false;
		recording=true;
		initStart();
		rankin=-1;
	}
	
	public void initStart() {
		tileField.fillCells(1,1,17,8,35);
		select=-1;
		paiCount=136;
		timeCount=0;
		logIndex=-1;
	}

	//実行
	public void run() {
		long sleepTime=0L;
		int  bfSelect=0;
		int  flag1=0;

		initApp();
		initGame();
		
		while (RunningFlag) {
		
			//コマンドイベントの処理
			switch (cmdnum) {
			case 0:
				for (int i=0;i<2;i++)
					removeCommand(commands[i]);
				switch (flag1) {
				case 1:
					RunningFlag=false;
					flag1=2;
					break;
				case 3:
					for (int i=0;i<10;i++) {
						hiscore[i]=107999;
						hiscoreDate[i]=0;
					}
					rankin=-1;
					saveData();
					addCommand(commands[2]);
					addCommand(commands[8]);
					flag1=0;
					break;
				}
				break;
			case 1:
				for (int i=0;i<2;i++)
					removeCommand(commands[i]);
				addCommand(commands[2]);
				if (select==10) {
					addCommand(commands[8]);
				} else {
					for (int i=3;i<8;i++)
						addCommand(commands[i]);
				}
				if ((flag1==1)&&(select>=0)&&(select<10)) {
					for (int j=1;j<9;j++)
						for (int i=1;i<18;i++)
							if (tileField.getCell(i,j)>0)
								tileField.setCell(i,j,field[i-1][j-1]);
				}
				flag1=0;
				break;
			case 2:
				for (int i=2;i<9;i++)
					removeCommand(commands[i]);
				addCommand(commands[0]);
				addCommand(commands[1]);
				flag1=1;
				if ((select>=0)&&(select<10)) {
					for (int j=1;j<9;j++)
						for (int i=1;i<18;i++)
							if (tileField.getCell(i,j)>0)
								tileField.setCell(i,j,35);
					drawGame();
				}
				break;
			case 3:
				newgame();
				initStart();
				recording=true;
				onetry=false;
				break;
			case 4:
				if ((select>=0)&&(select<3)) {
					recording=false;
					int x1,x2,y1,y2,k;
					k=0;
					for (y1=1;(y1<9)&&(k==0);y1++)
						for (x1=1;(x1<18)&&(k==0);x1++) {
							if (tileField.getCell(x1,y1)>0)
								for (y2=1;(y2<9)&&(k==0);y2++)
									for (x2=1;(x2<18)&&(k==0);x2++) {
										if (check(x1,y1,x2,y2)>=0){
											k=1;
											curX=x1; curY=y1;
											selX=x2; selY=y2;
											select=1;
										}
									}
						}
				}
				break;
			case 5:
				if ((logIndex>=0)&&(paiCount>0)&&(select>=0)) {
					int x1=paiLog[logIndex][0];
					int y1=paiLog[logIndex][1];
					int x2=paiLog[logIndex][2];
					int y2=paiLog[logIndex][3];
					tileField.setCell(x1,y1,field[x1-1][y1-1]);
					tileField.setCell(x2,y2,field[x2-1][y2-1]);
					paiCount+=2;
					logIndex--;
					select=0;
					recording=false;
				}
				break;
			case 6:
				if (onetry) {
					initStart();
					recording=false;
				}
				break;
			case 7:
				if (select!=10) {
					bfSelect=select;
					select=10;
					if (paiCount>0) rankin=-1;
					for (int i=3;i<8;i++)
						removeCommand(commands[i]);
					addCommand(commands[8]);
				}
				break;
			case 8:
				select=bfSelect;
				removeCommand(commands[8]);
				for (int i=3;i<8;i++)
					addCommand(commands[i]);
				break;
			}
			cmdnum=-999;
			
			if ((clearFlag==9)&&(select==10)) {
				flag1=3;
				clearFlag=0;
				removeCommand(commands[2]);
				removeCommand(commands[8]);
				addCommand(commands[0]);
				addCommand(commands[1]);
			}
			
			if (flag1==0) {
				
				switch (keyEvent) {
				case LEFT:
					if (select<4) {
						if (curX>1) curX--;
						else curX=17;
					}
					break;
				case RIGHT:
					if (select<4) {
						if (curX<17) curX++;
						else curX=1;
					}
					break;
				case UP:
					if (select<4) {
						if (curY>1) curY--;
						else curY=8;
					}
					break;
				case DOWN:
					if (select<4) {
						if (curY<8) curY++;
						else curY=1;
					}
					break;
				case FIRE:
					if (select==10) {
						select=bfSelect;
						removeCommand(commands[8]);
						for (int i=3;i<8;i++)
							addCommand(commands[i]);
					} else if (select<0) {
						for (int i=0;i<8;i++)
							for (int j=0;j<17;j++)
								tileField.setCell(j+1,i+1,field[j][i]);
						select=0;
						onetry=true;
						
					}else if (select==0) {
						int t=tileField.getCell(curX,curY);
						if ((t>0)&&(t<35)) {
							select=1;
							selX=curX;
							selY=curY;
						}
					} else {
						if (select==1) {
							lineX=new int[4][2];
							lineY=new int[4][2];
							for (int i=0;i<4;i++) {
								lineX[i][0]=selX;
								lineY[i][0]=selY;
								lineX[i][1]=curX;
								lineY[i][1]=curY;
							}
							//System.out.println("cur "+curX+","+curY);
							//System.out.println("sel "+selX+","+selY);
							int p=check(curX,curY,selX,selY);
							if (p>=0) {
								//tileField.setCell(curX,curY,0);
								//tileField.setCell(selX,selY,0);
								select=7;
								root=p-1;
								//System.out.println("root "+p+","+root);
							}
						}
					}
					break;
				case CLEAR:
					if (select==1) {
						select=0;
					} else if (select==10) {
						select=bfSelect;
						removeCommand(commands[8]);
						for (int i=3;i<8;i++)
							addCommand(commands[i]);
					}
	
					break;
				}
				keyEvent=-999;
				
				if ((select<3)&&(paiCount>0)&&(select>=0)) {
					if (timeCount<107999) {
						timeCount++;
						if (timeCount==107999) {
							recording=false;
						}
					}
				}
				
				if (select==4) {
					logIndex++;
					paiLog[logIndex][0]=curX;
					paiLog[logIndex][1]=curY;
					paiLog[logIndex][2]=selX;
					paiLog[logIndex][3]=selY;
					tileField.setCell(curX,curY,0);
					tileField.setCell(selX,selY,0);
					paiCount-=2;
					select=0;
					int x1,x2,y1,y2,k,p;
					k=0;
					for (y1=1;(y1<9)&&(k==0);y1++)
						for (x1=1;(x1<18)&&(k==0);x1++) {
							if (tileField.getCell(x1,y1)>0)
								for (y2=1;(y2<9)&&(k==0);y2++)
									for (x2=1;(x2<18)&&(k==0);x2++) {
										p=check(x1,y1,x2,y2);
										if (p>=0) {
											k=1;
										//System.out.println("p="+p+","+x1+","+y1+","+x2+","+y2);
										}
									}
						}
					//System.out.println("k="+k);
					if (k==0) {
						select=3;
					}
					if (paiCount==0) {
						rankin=-1;
						if (recording) {
							for (int i=0;i<10;i++) {
								if (timeCount<hiscore[i]) {
									for (int j=9;j>i;j--) {
										hiscore[j]=hiscore[j-1];
										hiscoreDate[j]=hiscoreDate[j-1];
									}
									hiscore[i]=timeCount;
									hiscoreDate[i]=sleepTime;
									rankin=i;
									saveData();
									break;
								}
							}
						}
					}
				}
			
				//描画
				drawGame();

			} else {
				keyEvent=-999;
				switch (flag1) {
				case 1:  drawComment("終了しますか？"); break;
				case 2:  drawComment("終了しています"); break;
				case 3:  drawComment("リセットしますか？"); break;
				}
			}
			
			flushGraphics();
            
			//スリープ
			while (System.currentTimeMillis()<sleepTime+100L);
			sleepTime=System.currentTimeMillis();
		}

		//終了
		if (CallFlag==false) Project017.myproject.finishGame();
		/*
		if (CallFlag==false) {
			//MIDletの破棄
			midlet.notifyDestroyed();
		}*/

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		if (select==10) {
			switch (keyCode) {
			case KEY_NUM1:
				if (clearFlag<3) clearFlag++;
				if ((clearFlag>5)&&(clearFlag<9)) clearFlag++;
				break;
			case KEY_NUM3:
				if ((clearFlag>2)&&(clearFlag<6)) clearFlag++;
				break;
			}
		}
		
		switch (keyCode) {
		case CLEAR:
			keyEvent=CLEAR;
			break;
		case SEND:
			keyEvent=SEND;
			break;
		default:
			keyEvent=getGameAction(keyCode);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//*
	//キーリピートイベント
	public void keyRepeated(int keyCode) {
		if (keyCode==0) return;
		switch (keyCode) {
		case CLEAR:
			keyEvent=CLEAR;
			break;
		case SEND:
			keyEvent=SEND;
			break;
		default:
			keyEvent=getGameAction(keyCode);
			break;
		}
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//キーリリースイベント
	public void keyReleased(int keyCode) {
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		int i;
		for (i=0;i<commands.length;i++)
			if (c==commands[i]) {
				cmdnum=i;
				break;
			}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//*
	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,128,255);
		g.drawRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,DISPLAY_WIDTH/2,DISPLAY_HEIGHT-18,Graphics.HCENTER|Graphics.TOP);
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//*
	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		RunningFlag=false;
		CallFlag=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private String getDate(long ms) {
		Date   d=new Date(ms);
		String s1=d.toString();
		int    nn=s1.length();
		String s2=s1.substring(4,19);
		String s3=s1.substring(nn-4,nn);
		return s3+" "+s2;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	private String getTime(long ms) {
		Date   d=new Date(ms);
		String s1=d.toString();
		return s1.substring(11,19);
	}
*/
	
	private String getTime(int count) {
		int h=count/36000;
		int m=(count%36000)/600;
		int s=((count%36000)%600)/10;
		return Integer.toString(h)+":"+(Integer.toString(m+100)).substring(1,3)+":"
		          +(Integer.toString(s+100)).substring(1,3)+"."+Integer.toString(count%10);
		/*
		String s1="0"+Integer.toString(m);
		String s2="0"+Integer.toString(s);
		int n1=s1.length();
		int n2=s2.length();
		return Integer.toString(h)+":"+s1.substring(n1-2,n1)+":"
		                  +s2.substring(n2-2,n2)+"."+Integer.toString(count%10);
		//*/                
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void newgame() {
		int      i,j,k,x,y,tmp;
		//int[][]  field=new int[17][8];

		//初期化
		for (i=0;i<8;i++)
			for (j=0;j<17;j++)
				field[j][i]=(j+i*17)/4+1;
		
		//シャッフル
		for (k=0;k<15;k++)
			for (i=0;i<8;i++)
				for (j=0;j<17;j++) {
					x=rand(17);
					y=rand(8);
					tmp=field[x][y];
					field[x][y]=field[j][i];
					field[j][i]=tmp;
				}
		
		//セット
		//tileField.fillCells(1,1,17,8,35);
		/*
		for (i=0;i<8;i++)
			for (j=0;j<17;j++)
				tileField.setCell(j+1,i+1,field[j][i]);
		//*/
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private int seek(int x1, int y1, int x2, int y2, int dx, int dy, int c, int t) {
		if ((x1==x2)&&(y1==y2)) return 0;
		if ((x1<0)||(x1>18)||(y1<0)||(y1>9)) return -1;
		if (tileField.getCell(x1,y1)>0) return -1;
		int r=-1;
		if (c>0) {
			if ((dx!=0)&&(y1!=y2)) {
				int cy;
				if (y2<y1) cy=-1; else cy=1;
				r=seek(x1,y1+cy,x2,y2,0,cy,c-1,t);
			} 
			if ((dy!=0)&&(x1!=x2)) {
				int cx;
				if (x2<x1) cx=-1; else cx=1;
				r=seek(x1+cx,y1,x2,y2,cx,0,c-1,t);
			}
		}
		if (r<0) {
			r=seek(x1+dx,y1+dy,x2,y2,dx,dy,c,t);
		} else {
			lineX[t][c-1]=x1;
			lineY[t][c-1]=y1;
			//System.out.println("seek! "+x1+","+y1);
		}
		if (r<0) {
			return -1;
		} else {
			return r+1;
		}
	}

	private int check(int x1, int y1, int x2, int y2) {
		if ((x1!=x2)||(y1!=y2)) {
			int t1=tileField.getCell(x1,y1);
			int t2=tileField.getCell(x2,y2);
			if (t1==t2) {
				int dx=Math.abs(x1-x2);
				int dy=Math.abs(y1-y2);
				int d=dx+dy;
				if (d==1) {
					return 0;
				} else if (d>1) {
					int[] r=new int[4];
					r[0]=seek(x1+1,y1,x2,y2,1,0,2,0);
					r[1]=seek(x1,y1+1,x2,y2,0,1,2,1);
					r[2]=seek(x1-1,y1,x2,y2,-1,0,2,2);
					r[3]=seek(x1,y1-1,x2,y2,0,-1,2,3);
					int i,k=-10,n=100;;
					for (i=0;i<4;i++)
						if ((r[i]>=0)&&(r[i]<n)) {
							k=i;
							n=r[i];
						}
					return k+1;
				}
			}
		}
		return -1;
	}
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawHiscore() {
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(255,255,255);
		g.drawString("順位",0,25,Graphics.LEFT|Graphics.TOP);
		g.drawString("クリア時間",35,25,Graphics.LEFT|Graphics.TOP);
		g.drawString("日付",115,25,Graphics.LEFT|Graphics.TOP);
		for (int i=0;i<10;i++) {
			if (i==rankin)
				g.setColor(rand(256),rand(256),rand(256));
			else
				g.setColor(255,255,255);
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.drawString(Integer.toString(i+1),20,45+i*18,Graphics.RIGHT|Graphics.TOP);
			g.drawString(getTime(hiscore[i]),105,45+i*18,Graphics.RIGHT|Graphics.TOP);
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
			g.drawString(getDate(hiscoreDate[i]),115,49+i*18,Graphics.LEFT|Graphics.TOP);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void drawGame() {
		g.setColor(0,128,0);
		g.fillRect(0,0,DISPLAY_WIDTH,DISPLAY_HEIGHT);
		g.drawImage(images[0],0,0,Graphics.LEFT|Graphics.TOP);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
		g.setColor(0,0,0);
		g.drawString(appver,61,6,Graphics.LEFT|Graphics.TOP);
		
		g.setColor(255,255,255);
		g.fillRect(0,252,240,16);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		if (recording)
			g.setColor(0,0,0);
		else
			g.setColor(255,0,0);
		g.drawString("時間: "+getTime(timeCount),1,252,Graphics.LEFT|Graphics.TOP);
		g.setColor(0,0,0);
		g.drawString("残り: "+paiCount,141,252,Graphics.LEFT|Graphics.TOP);

		if (select==10) {
			drawHiscore();
		
		} else {

			if ((select>=5)&&(select<=7)) {
				if (root>=0) {
					g.setColor(255,255,0);
					g.drawLine(tfLeft+6+12*curX          ,tfTop+7+15*curY,
					           tfLeft+6+12*lineX[root][1],tfTop+7+15*lineY[root][1]);
					g.drawLine(tfLeft+6+12*lineX[root][0],tfTop+7+15*lineY[root][0],
					           tfLeft+6+12*lineX[root][1],tfTop+7+15*lineY[root][1]);
					g.drawLine(tfLeft+6+12*lineX[root][0],tfTop+7+15*lineY[root][0],
					           tfLeft+6+12*selX          ,tfTop+7+15*selY);
				}
				select--;
			}

			tileField.paint(g);
			
			if (select==1) {
				g.setColor(0,255,255);
				g.drawRect(selX*12+tfLeft,selY*15+tfTop,11,14);
				g.drawRect(selX*12+tfLeft+1,selY*15+tfTop+1,9,12);
			}
			
			if (paiCount>0) {
				if (select==0) 
					g.setColor(0,255,255);
				else
					g.setColor(0,255,0);
				g.drawRect(curX*12+tfLeft,curY*15+tfTop,11,14);
				g.drawRect(curX*12+tfLeft-1,curY*15+tfTop-1,13,16);
			}
			
			if ((select>=4)&&(select<=6)) {
				g.setColor(255,255,0);
				g.drawRect(selX*12+tfLeft,selY*15+tfTop,11,14);
				g.drawRect(selX*12+tfLeft-1,selY*15+tfTop-1,13,16);
				g.drawRect(curX*12+tfLeft,curY*15+tfTop,11,14);
				g.drawRect(curX*12+tfLeft-1,curY*15+tfTop-1,13,16);
			}
			
			if (select==3) {
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
				g.setColor(255,255,255);
				if (paiCount>0) {
					g.drawString("手詰まりです",0,235,Graphics.LEFT|Graphics.TOP);
				} else {
					g.drawString("おめでとうございます",0,235,Graphics.LEFT|Graphics.TOP);
					drawHiscore();
				}
			}
			
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void saveData() {
		MyRecord.saveInts(RS_SCORE,hiscore);
		MyRecord.saveLongs(RS_DATE,hiscoreDate);
	}
	
	private void loadData() {
		int[] dint=MyRecord.loadInts(RS_SCORE);
		if (dint!=null) hiscore=dint;
		long[] dlong=MyRecord.loadLongs(RS_DATE);
		if (dlong!=null) hiscoreDate=dlong;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Canvas1の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
