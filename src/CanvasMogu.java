// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// CanvasMogu.java   ゲームのメイン処理
//
// もぐらたたき ver 1.7 (C)Tomarky 2009.07.05-2009.07.06
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import java.util.*;
import javax.microedition.rms.*;
import java.io.*;
import javax.microedition.media.*;//【追加】
import MyPackage.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//ゲームメイン処理(キャンバス)
class CanvasMogu extends Canvas1 implements CommandListener {
	private final String RS_SCORE="tm.mogu.hiscore";
	private final String GAME_VERSION="ver 1.8";

	private final int   rm_TITLE=0,rm_GAME=1,rm_COUNT=2,rm_END=3;
	private int         RunMode=rm_TITLE;
	
	private Graphics    g;
	private Image[]     imgs=new Image[4];
	private int         keyEvent=-999;          //キーイベント
	private int         keyState;               //キー状態
	private boolean     bl=true;
	private boolean     cf=false;
	private Command[]   cmds=new Command[5];    //コマンド
	private int         cmd=-1;
	private int         w=0;
	private int         h=0;
	private Random      rndm=new Random();      //乱数

	private int[]       mogu=new int[9];
	private int         score=0;
	private int         hiscore=0;
	private int         timecount=0;
	private int         gamecount=0;
	private boolean     flag1=false;
	private boolean		flag2=false;
	private boolean		flag3=false;
	//private boolean		usesound=true;
	private boolean     seefps=false;
	private int         fpsroot=0;
	
	private int         n=0;

    //private Player[] player=new Player[4];//プレイヤー【追加】
 
	private MyPlayer   myplayer=null;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasMogu() {
		//キーイベントの抑制
		super(false);
		
		//画面サイズ
		w=240;
		h=268;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//実行
	public void run() {
		long sleepTime=0L;
		long test=0L;
		long fps=0L;
		int a=0;
		int k=-1;
		int c1=0;
		int c2=0;
		int c3=0;
		int i,j;
		
		//画像の読み込み
		try { 
			for (i=0;i<imgs.length;i++) {
				imgs[i]=Image.createImage("/mogu0"+Integer.toString(i+1)+".png");
			}
		} catch (Exception e) {
			System.out.println("CanvasMogu.run(createImage)::"+e.toString());
		}
		
		String[] se=new String[4];
		for (i=0;i<se.length;i++) {
			se[i]="/moguse0"+i+".mid";
		}
		myplayer=new MyPlayer(se,MyPlayer.TYPE_MIDI);
		for (i=0;i<se.length;i++)
			myplayer.prefetch(i);
		
		hiscore=MyRecord.loadInt(RS_SCORE,0);
		
		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		cmds[0]=new Command("はい",  Command.OK,    0);
		cmds[1]=new Command("いいえ",Command.CANCEL,0);
		cmds[2]=new Command("終了",  Command.SCREEN,0);
		cmds[3]=new Command("ﾘｾｯﾄ",  Command.SCREEN,2);
		cmds[4]=new Command("効果音",Command.SCREEN,1);

		addCommand(cmds[2]);
		addCommand(cmds[3]);
		addCommand(cmds[4]);
		

		//コマンドリスナーの指定
		setCommandListener(this);

/*
        //サウンドファイルの読み込み【追加】
        InputStream in;
        try {
            //WAVE1
	        in=getClass().getResourceAsStream("/se01.wav");
	        player[0]=Manager.createPlayer(in,"audio/x-wav");
	        player[0].prefetch();
            //WAVE2
	        in=getClass().getResourceAsStream("/se02.wav");
	        player[1]=Manager.createPlayer(in,"audio/x-wav");
	        player[1].prefetch();
            //WAVE3
	        in=getClass().getResourceAsStream("/se04.wav");
	        player[2]=Manager.createPlayer(in,"audio/x-wav");
	        player[2].prefetch();
            //WAVE4
	        in=getClass().getResourceAsStream("/se05.wav");
	        player[3]=Manager.createPlayer(in,"audio/x-wav");
	        player[3].prefetch();
        } catch (Exception e) {
            System.out.println(e.toString());
        }
*/
		//メインループ
		while (bl) {
			timecount++;
			if (timecount>=100) { timecount=0;}
			
			
			//コマンドイベント処理
			switch (cmd) {
			case 0:
				if (flag1) { bl=false;}
				if (flag2) {
					removeCommand(cmds[0]);
					removeCommand(cmds[1]);
					addCommand(cmds[2]);
					addCommand(cmds[3]);
					addCommand(cmds[4]);
					flag2=false;
					timecount=0;
					hiscore=0;
					MyRecord.saveInt(RS_SCORE,hiscore);
					myplayer.start(2);
					//if (usesound) { playSound(2);}
				}
				if (flag3) {
					removeCommand(cmds[0]);
					removeCommand(cmds[1]);
					addCommand(cmds[2]);
					addCommand(cmds[3]);
					addCommand(cmds[4]);
					flag3=false;
					//usesound=true;
					timecount=0;
					myplayer.setUse(true);
					myplayer.start(3);
					//if (usesound) { playSound(3);}
				}
				break;
			case 1:
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				addCommand(cmds[2]);
				addCommand(cmds[3]);
				addCommand(cmds[4]);
				flag1=false;
				flag2=false;
				if (flag3) {
					flag3=false;
					//usesound=false;
					myplayer.setUse(false);
				}
				timecount=0;
				fps=0L;
				break;
			case 2:
				flag1=true;
				removeCommand(cmds[2]);
				removeCommand(cmds[3]);
				removeCommand(cmds[4]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			case 3:
				flag2=true;
				removeCommand(cmds[2]);
				removeCommand(cmds[3]);
				removeCommand(cmds[4]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			case 4:
				flag3=true;
				removeCommand(cmds[2]);
				removeCommand(cmds[3]);
				removeCommand(cmds[4]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			}
			cmd=-1;
			
			//キーイベント処理
			if ((flag1)||(flag2)||(flag3)) { keyEvent=-999;}
			switch (RunMode) {
			case rm_TITLE:
				if (keyEvent==FIRE) {
					removeCommand(cmds[2]);
					removeCommand(cmds[3]);
					removeCommand(cmds[4]);
					newgame();
					RunMode=rm_COUNT;
					timecount=1;
					gamecount=3;
					myplayer.start(0);
					//if (usesound) { playSound(0);}
				}
				break;
			case rm_END:
				if (keyEvent==FIRE) {
					if (score>hiscore) {
						hiscore=score;
						MyRecord.saveInt(RS_SCORE,hiscore);
					}
					RunMode=rm_TITLE;
					addCommand(cmds[2]);
					addCommand(cmds[3]);
					addCommand(cmds[4]);
					timecount=0;
				}
				break;
			}
			keyEvent=-999;

			//キー状態の処理
			if (RunMode==rm_GAME) {
				keyState=getKeyStates();
				if ((GAME_A_PRESSED & keyState)!=0) { attack(1);}
				if ((UP_PRESSED     & keyState)!=0) { attack(2);}
				if ((GAME_B_PRESSED & keyState)!=0) { attack(3);}
				if ((LEFT_PRESSED   & keyState)!=0) { attack(4);}
				if ((FIRE_PRESSED   & keyState)!=0) { attack(5);}
				if ((RIGHT_PRESSED  & keyState)!=0) { attack(6);}
				if ((GAME_C_PRESSED & keyState)!=0) { attack(7);}
				if ((DOWN_PRESSED   & keyState)!=0) { attack(8);}
				if ((GAME_D_PRESSED & keyState)!=0) { attack(9);}
			}
			
			//ゲーム処理
			switch (RunMode) {
			case rm_GAME:
				if (timecount%20==0) {
					gamecount--;
					if (gamecount==0) {
						myplayer.start(1);
						//if (usesound) { playSound(1);}
					} else if ((gamecount>0)&&(gamecount<4)) {
						myplayer.start(0);
						//if (usesound) { playSound(0);}
					}
				}
				if (gamecount<0) {
					RunMode=rm_END;
					if (score>hiscore) {
						myplayer.start(3);
						//if (usesound) { playSound(3);}
					}
				} else {
					if (timecount%6==0) {
						if (n<4) {
							a=rand(9);
							if (mogu[a]==0) {
								mogu[a]=1;
								n++;
							}
						}
					}
					for (i=0;i<9;i++) {
						if (mogu[i]>0) {
							mogu[i]++;
							if (mogu[i]>16) {
								mogu[i]=0;
								n--;
							}
						} else if (mogu[i]<0) {
							mogu[i]--;
							if (mogu[i]<-10) {
								mogu[i]=0;
							}
						}
					}
				}				
				break;
			case rm_COUNT:
				if (timecount%20==0) {
					gamecount--;
					if (gamecount>0) {
						myplayer.start(0);
						//if (usesound) { playSound(0);}
					} else if (gamecount==0) {
						myplayer.start(1);
						//if (usesound) { playSound(1);}
					}
				}
				if (gamecount<0) {
					timecount=1;
					gamecount=30;
					n=0;
					RunMode=rm_GAME;
				}
				break;
			}
			
			//描画
			g.setColor( 0,255,0);
			g.fillRect(0,0,w,h);
			switch (RunMode) {
			case rm_TITLE:
				if ((timecount>90)||(flag1)||(flag2)||(flag3)) {
					g.drawImage(imgs[1],w/2,50,Graphics.HCENTER|Graphics.TOP);
				} else if (timecount>70) {
					g.drawImage(imgs[0],w/2,50,Graphics.HCENTER|Graphics.TOP);
				} else if (timecount>45) {
					g.drawImage(imgs[3],w/2,50,Graphics.HCENTER|Graphics.TOP);
				} else {
					g.drawImage(imgs[2],w/2,50,Graphics.HCENTER|Graphics.TOP);
				}
				//タイトル文字
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
				g.setColor(255,255,255);
				for (i=-1;i<2;i++) {
					for (j=-1;j<2;j++) {
						g.drawString("もぐらたたき",w/2+j,h/2+i,Graphics.HCENTER|Graphics.TOP);
					}
				}
				g.setColor(255,0,0);
				g.drawString("もぐらたたき",w/2,h/2,Graphics.HCENTER|Graphics.TOP);
				//ハイスコア
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
				g.setColor(255,255,255);
				g.drawString(GAME_VERSION,w/2,h/2+20,Graphics.HCENTER|Graphics.TOP);
				g.drawString("HISCORE = "+hiscore,w/2,h/2+50,Graphics.HCENTER|Graphics.TOP);
				//入力促し文字
				if (((timecount/10)%2==0)&&(flag1==false)&&(flag2==false)&&(flag3==false)) {
					g.setColor(255,255,0);
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
					g.drawString("Press（●）Key",w/2,h/2+80,Graphics.HCENTER|Graphics.TOP);
				}
				g.setColor(255,255,255);
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
				//if (usesound) {
				if (myplayer.getUse()) {
					g.drawString("効果音:on ",w,h,Graphics.RIGHT|Graphics.BOTTOM);
				} else {
					g.drawString("効果音:off",w,h,Graphics.RIGHT|Graphics.BOTTOM);
				}
				break;
			case rm_GAME:
				//スコア
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
				g.setColor(255,255,255);
				g.drawString("SCORE = "+score,0,0,Graphics.LEFT|Graphics.TOP);
				for (i=0;i<9;i++) {
					if (mogu[i]==0) {
						g.drawImage(imgs[0],(i%3+1)*w/4,(i/3+1)*h/4,Graphics.HCENTER|Graphics.VCENTER);
					} else if (mogu[i]<0) {					
						g.drawImage(imgs[3],(i%3+1)*w/4,(i/3+1)*h/4,Graphics.HCENTER|Graphics.VCENTER);
					} else if ((mogu[i]>12)||(mogu[i]<4)) {
						g.drawImage(imgs[1],(i%3+1)*w/4,(i/3+1)*h/4,Graphics.HCENTER|Graphics.VCENTER);
					} else {
						g.drawImage(imgs[2],(i%3+1)*w/4,(i/3+1)*h/4,Graphics.HCENTER|Graphics.VCENTER);
					}
				}
				break;
			case rm_COUNT:
				//スコア
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
				g.setColor(255,255,255);
				g.drawString("SCORE = "+score,0,0,Graphics.LEFT|Graphics.TOP);
				//もぐら穴
				for (i=0;i<3;i++) {
					g.drawImage(imgs[0],(i+1)*w/4,  h/4,Graphics.HCENTER|Graphics.VCENTER);
					g.drawImage(imgs[0],(i+1)*w/4,2*h/4,Graphics.HCENTER|Graphics.VCENTER);
					g.drawImage(imgs[0],(i+1)*w/4,3*h/4,Graphics.HCENTER|Graphics.VCENTER);
				}
				if (gamecount>0) {
					//カウント
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
					g.setColor(255,255,255);
					for (i=-1;i<2;i++) {
						for (j=-1;j<2;j++) {
							g.drawString(""+gamecount,w/2+j,h/2+i,Graphics.HCENTER|Graphics.TOP);
						}
					}
					g.setColor(255,0,0);
					g.drawString(""+gamecount,w/2,h/2,Graphics.HCENTER|Graphics.TOP);
				} else {
					//カウント終了
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
					g.setColor(255,255,255);
					for (i=-1;i<2;i++) {
						for (j=-1;j<2;j++) {
							g.drawString("スタート",w/2+j,h/2+i,Graphics.HCENTER|Graphics.TOP);
						}
					}
					g.setColor(255,0,0);
					g.drawString("スタート",w/2,h/2,Graphics.HCENTER|Graphics.TOP);
				}
				break;
			case rm_END:
				//スコア
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
				g.setColor(255,255,255);
				g.drawString("SCORE = "+score,0,0,Graphics.LEFT|Graphics.TOP);
				//もぐら穴
				for (i=0;i<3;i++) {
					g.drawImage(imgs[0],(i+1)*w/4,  h/4,Graphics.HCENTER|Graphics.VCENTER);
					g.drawImage(imgs[0],(i+1)*w/4,2*h/4,Graphics.HCENTER|Graphics.VCENTER);
					g.drawImage(imgs[0],(i+1)*w/4,3*h/4,Graphics.HCENTER|Graphics.VCENTER);
				}
				//カウント
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
				g.setColor(255,255,255);
				for (i=-1;i<2;i++) {
					for (j=-1;j<2;j++) {
						g.drawString("Game Over",w/2+j,h/2+i,Graphics.HCENTER|Graphics.TOP);
					}
				}
				//終了文字
				g.setColor(255,0,0);
				g.drawString("Game Over",w/2,h/2,Graphics.HCENTER|Graphics.TOP);
				//ハイスコア
				if (score>hiscore) {
					g.setColor(255,255,255);
					g.fillRect(w/2-70,h/2+40,140,40);
					if ((timecount%3)==0) {
						c1=rand(256); c2=rand(256); c3=rand(256);
					}
					g.setColor(c1,c2,c3);
					g.drawString("You Got HiScore!!",w/2,h/2+40,Graphics.HCENTER|Graphics.TOP);
					g.drawString("Score = "+score,w/2,h/2+60,Graphics.HCENTER|Graphics.TOP);
				}
				//入力促し文字
				if ((timecount/10)%2==0) {
					g.setColor(255,255,0);
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
					g.drawString("Press（●）Key",w/2,h/2+100,Graphics.HCENTER|Graphics.TOP);
				}
				break;
			}
			if (flag1) { drawComment("終了しますか？");}
			if (flag2) { drawComment("ハイスコアをリセットしますか？");}
			if (flag3) { drawComment("効果音を使いますか？");}
			
			if (seefps) {
				g.setColor(255,255,255);
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
				g.drawString("SPF("+fps+"/"+test+")",0,h,Graphics.LEFT|Graphics.BOTTOM);
			}

			flushGraphics();
            
			if (seefps) {
				test=System.currentTimeMillis()-sleepTime;
				if (test>fps) {fps=test;}
			}
            
            
			//スリープ
			while (System.currentTimeMillis()<sleepTime+50);// &&
//					System.currentTimeMillis()-sleepTime<50);
			sleepTime=System.currentTimeMillis();
		}
		myplayer.close();
		
		if (cf==false) Project017.myproject.finishGame();
		//if (cf==false) Project002.midlet.notifyDestroyed();

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		switch (RunMode) {
		case rm_TITLE:
			if (keyCode==-5)
				keyEvent=FIRE;
			break;
		case rm_GAME:
			switch (keyCode) {
			case KEY_NUM1:
			case KEY_NUM2:
			case KEY_NUM3:
			case KEY_NUM4:
			case KEY_NUM5:
			case KEY_NUM6:
			case KEY_NUM7:
			case KEY_NUM8:
			case KEY_NUM9:
				attack(keyCode-48);
				break;
			}
			break;
		case rm_END:
			switch (keyCode) {
			case -5:
				keyEvent=FIRE;
				break;
			case KEY_NUM1:
				if (fpsroot==0) {fpsroot++;}
				break;
			case KEY_NUM2:
				if (fpsroot==1) {fpsroot++;}
				break;
			case KEY_NUM3:
				if (fpsroot==2) {fpsroot++;}
				break;
			case KEY_NUM4:
				if (fpsroot==3) {fpsroot++;}
				break;
			case KEY_NUM5:
				if (fpsroot==4) {fpsroot++;}
				break;
			case KEY_NUM6:
				if (fpsroot==5) {fpsroot++;}
				break;
			case KEY_NUM7:
				if (fpsroot==6) {fpsroot++;}
				break;
			case KEY_NUM8:
				if (fpsroot==7) {fpsroot++;}
				break;
			case KEY_NUM9:
				if (fpsroot==8) {
					fpsroot=0;
					seefps=!seefps;
				}
				break;
			}
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キーリピートイベント
	public void keyRepeated(int keyCode) {
		if (keyCode==0) return;
		
		if (RunMode==rm_GAME) {
			switch (keyCode) {
			case KEY_NUM1:
			case KEY_NUM2:
			case KEY_NUM3:
			case KEY_NUM4:
			case KEY_NUM5:
			case KEY_NUM6:
			case KEY_NUM7:
			case KEY_NUM8:
			case KEY_NUM9:
				attack(keyCode-48);
				break;
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		if (c==cmds[0]) cmd=0;
		if (c==cmds[1]) cmd=1;
		if (c==cmds[2]) cmd=2;
		if (c==cmds[3]) cmd=3;
		if (c==cmds[4]) cmd=4;
		//repaint();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	private void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,h-20,w,20);
		g.setColor(0,128,255);
		g.drawRect(0,h-20,w,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,w/2,h-18,Graphics.HCENTER|Graphics.TOP);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ニューゲーム
	private void newgame() {
		for (int i=0;i<9;i++) {
			mogu[i]=0;
		}
		score=0;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//たたく
	private void attack(int k) {
		if ((k>0)&&(k<10)){
			if (mogu[k-1]>0) {
				mogu[k-1]=-1;
				score+=10;
				n--;
				myplayer.start(2);
				//if (usesound) { playSound(2);}
			}
		}
	}


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void callFinish() {
		bl=false;
		cf=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //CanvasMoguの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
