// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// Project017.java  アプリ本体
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
//import javax.microedition.lcdui.game.*;
//import java.util.*;
//import javax.microedition.rms.*;
//import java.io.*;
//import javax.microedition.media.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import MyPackage.MyRecord;
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//MIDlet(アプリ本体)
public class Project017 extends MIDlet implements CommandListener {
	public  static Project017   myproject;
	private Display             display;
	private Canvas1             canvas1=null;
	private Thread              thread;
	private Command[]           cmds=new Command[1];
	private List                list;
	
	private final String        RS_GAMELIST="tm.gamebox.gamelist";
	private final int           GAMECOUNT=7;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	public Project017() {
		myproject=this;
		
		display=Display.getDisplay(this);
		
		list=new List("遊びたいゲームを選んでください",Choice.IMPLICIT);
		
		list.addCommand(cmds[0]=new Command("終了",Command.SCREEN,0));
		//list.addCommand(cmds[1]=new Command("起動",Command.SCREEN,1));
		
		list.setCommandListener(this);
		
		Image img=null;
		
		String gametitle;
		int    i=0;
		
		byte[] gamelist=MyRecord.loadBytes(RS_GAMELIST);
		if (gamelist==null) {
			while (appendList(i,-1)) {
				i++;
			}
		} else {
			if (gamelist.length<GAMECOUNT) {
				while (appendList(i,-1)) {
					i++;
				}
			} else {
				for (i=0;i<gamelist.length;i++) {
					appendList((int)gamelist[i],-1);
				}
			}
		}
		
		display.setCurrent(list);

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アプリの開始 (起動時、一時停止からの復旧時に実行される)
	public void startApp() {
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アプリの一時停止
	public void pauseApp() {
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//アプリの終了
	public void destroyApp(boolean flag) {
		if (canvas1!=null) {
			canvas1.callFinish();
			try {thread.join();} catch (Exception e) {}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	public void commandAction(Command c, Displayable disp) {
		if (c==cmds[0]) {
			int    size=list.size();
			byte[] gamelist=new byte[size];
			for (int i=0;i<size;i++) {
				gamelist[i]=(byte)getIndex(list.getString(i));
			}
			MyRecord.saveBytes(RS_GAMELIST,gamelist);
			
			notifyDestroyed();
		} else if (c==List.SELECT_COMMAND)  {
			System.gc();
			int p=list.getSelectedIndex();
			
			String gametitle=list.getString(p);
			
			int index=getIndex(gametitle);
			
			canvas1=newGameCanvas(index);
			if (canvas1==null) {
				return;
			}
			
			display.setCurrent(canvas1);
			thread=new Thread(canvas1);
			//スレッドの実行
			try { thread.start();} catch (Exception e) {}
			
			if (p>0) {
				list.delete(p);
				appendList(index,0);
				list.setSelectedIndex(0,true);
				System.gc();
			}
			
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void finishGame() {
		display.setCurrent(list);
		canvas1=null;
		thread=null;
		//notifyDestroyed();
		System.gc();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private boolean appendList(int index, int elementNum) {
		String gametitle=gameTitle(index);
		if (gametitle==null) {
			return false; 
		}
		Image  img=null;
		try {
			img=Image.createImage(gameIcon(index));
		} catch (Exception e) {
			System.out.println("MyProject017(createImage)::"+e.toString());
			img=null;
		}
		if (elementNum<0) {
			list.append(gametitle,img);
		} else {
			list.insert(elementNum,gametitle,img);
		}
		return true;
	}

	private String gameTitle(int index) {
		switch (index) {
		case 0:  return "もぐらたたき";
		case 1:  return "マインスイーパEasy";
		case 2:  return "Snake";
		case 3:  return "Like Dr.Mario";
		case 4:  return "Block Line";
		case 5:  return "四川省";
		case 6:  return "SPEED DEMON";
		default: return null;
		}
	}
	
	private String gameIcon(int index) {
		switch (index) {
		case 0:  return "/iconMogu.png";
		case 1:  return "/iconMine.png";
		case 2:  return "/iconSnake.png";
		case 3:  return "/iconDrM.png";
		case 4:  return "/iconBlockLine.png";
		case 5:  return "/iconSisen.png";
		case 6:  return "/iconSpeedDmn.png";
		default: return null;
		}
	}
	
	private Canvas1 newGameCanvas(int index) {
		switch (index) {
		case 0:  return new CanvasMogu();
		case 1:  return new CanvasMine();
		case 2:  return new CanvasSnake();
		case 3:  return new CanvasDrM();
		case 4:  return new CanvasBlockLine();
		case 5:  return new CanvasSisen();
		case 6:  return new CanvasSpeedDmn();
		default: return null;
		}
	}
	
	private int getIndex(String gametitle) {
		int    i=0;
		String str1;
		while ((str1=gameTitle(i))!=null) {
			if (gametitle.equals(str1)) {
				return i;
			}
			i++;
		}
		return -1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //Project017の終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
