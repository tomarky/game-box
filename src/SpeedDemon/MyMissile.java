// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyMissile.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
package SpeedDemon;

// インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
final class MyMissile {
	public Sprite sp=null;
	private boolean running=false;
	private int     posy=1,speed=0;

	//コンストラクタ
	MyMissile() {
		try {
			this.sp=new Sprite(Image.createImage("/missile.png"),10,4);
		} catch (Exception e) {
			System.out.println("Error MyMissile.MyMissile()");
			System.out.println(e.toString());
		}
		int[] an={0,1};
		this.sp.setFrameSequence(an);
		this.sp.setVisible(false);
	}

	public void move(int v) {
		if (this.running){
			this.sp.move(this.speed-v,0);
			if ((this.sp.getX()>240)||(this.sp.getX()<=-10)) {
				this.running=false;
				this.sp.setVisible(false);
			}
			this.sp.nextFrame();
		}
	}

	public void paint(Graphics g) {
		this.sp.paint(g);
	}	
	
	public void setPosition(int ps) {
		this.sp.setPosition(this.sp.getX(),ps+5);
		/*
		switch (ps) {
		case 0:
			this.sp.setPosition(this.sp.getX(),55);
			this.posy=0;
			break;
		case 2:
			this.sp.setPosition(this.sp.getX(),115);
			this.posy=2;
			break;
		default:
			this.sp.setPosition(this.sp.getX(),85);
			this.posy=1;
			break;
		}*/
	}
	
	public void crash() {
		this.running=false;
		this.sp.setVisible(false);
	}
	
	public boolean isRunning() {
		return this.running;
	}

	public void startRunning(int x,int spd) {
		this.running=true;
		this.speed=spd;
		this.sp.setPosition(x, this.sp.getY());
		this.sp.setVisible(true);
	}



// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyMissileの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
