// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyCar.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
package SpeedDemon;

// インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
final class MyCar {
	private Sprite  sp=null;
	private int     speed=0,vy=0,
	                posy=1;
	private boolean running=false,
	                crashed=false;

	//コンストラクタ
	MyCar() {
		try {
			this.sp=new Sprite(Image.createImage("/car.png"),18,11);
		} catch (Exception e) {
			System.out.println("Error Mycar.MyCar()");
			System.out.println(e.toString());
		}
		this.setColor(0);
		this.sp.setVisible(false);
	}
	MyCar(MyCar car) {
		this.sp=new Sprite(car.sp);
		this.setColor(0);
		this.sp.setVisible(false);
	}
	
	public void move(int v) {
		if (this.running){
			this.sp.move(this.speed-v,vy);
			if (vy!=0) {
				int yy=this.sp.getY();
				if ((yy==50)||(yy==80)||(yy==110)) {
					vy=0;
				}
			}
			if (this.sp.getX()<=-18) {
				this.running=false;
				this.sp.setVisible(false);
			} else if (this.sp.getX()>240) {
				this.sp.setPosition(240,this.sp.getY());
			}
			this.sp.nextFrame();
		}
	}
	
	public void movePosition(int mv) {
		int yy=this.sp.getY();
		if (mv<0) {
			if (yy>50)
				vy=-5;
		} else {
			if (yy<110)
				vy=5;
		}
		//this.setPosition(this.posy+mv);
	}

	public void setPosition(int ps) {
		switch (ps) {
		case 0:
			this.sp.setPosition(this.sp.getX(),50);
			this.posy=0;
			break;
		case 2:
			this.sp.setPosition(this.sp.getX(),110);
			this.posy=2;
			break;
		default:
			this.sp.setPosition(this.sp.getX(),80);
			this.posy=1;
			break;
		}
	}

	
	public void paint(Graphics g) {
		this.sp.paint(g);
	}	
	
	public void setColor(int color) {
		int[] an={color,color+4,color+8,color+12};
		this.sp.setFrameSequence(an);
	}
	
	public void setSpeed(int newspeed) {
		this.speed=newspeed;
	}
	
	public void stopRunning() {
		this.running=false;
	}
	
	public void crash() {
		this.vy=0;
		this.speed=0;
		this.setColor(3);
		this.crashed=true;
	}
	
	public void setVisible(boolean vis) {
		this.sp.setVisible(vis);
	}
	
	public void startRunning(int x) {
		this.running=true;
		this.crashed=false;
		this.sp.setPosition(x, this.sp.getY());
		this.sp.setVisible(true);
	}
	
	public boolean isRunning() {
		return this.running;
	}
	
	public boolean isCrashed() {
		return this.crashed;
	}
	
	/*
	public int getPosition() {
		return this.posy;
	}*/
	
	public int getY() {
		return this.sp.getY();
	}
	
	public boolean collidesWith(MyCar car) {
		if ((this.running)&&(car.running)) {
			if ((this.crashed&&car.crashed)==false) {
				if (this.sp.collidesWith(car.sp,false)) {
					return this.sp.collidesWith(car.sp,true);
				}
			}
		} 
		return false;
	}

	public boolean collidesWith(MyMissile mis) {
		if (this.running) {
			if (this.sp.collidesWith(mis.sp,false)) {
				return this.sp.collidesWith(mis.sp,true);
			}
		} 
		return false;
	}
	
	
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyCarの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
