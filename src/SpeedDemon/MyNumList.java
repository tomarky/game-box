// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyNumList.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
package SpeedDemon;

// インポート
//import javax.microedition.lcdui.game.*;
//import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
final class MyNumList {
	
	private int[] list=null;
	private int   counter=0;
	
	//コンストラクタ
	MyNumList(int n,int m) {
		this.list=new int[n];
		for (int i=0;i<n;i++) {
			this.list[i]=i%m;
		}
	}
	
	private void makeNewList() {
		int i,j,t,n;
		n=this.list.length;
		for (i=0;i<n;i++) {
			j=this.rand(n);
			t=this.list[i];
			this.list[i]=this.list[j];
			this.list[j]=t;
		}
		
	}

	public int getNext() {
		if (this.counter==0) {
			this.makeNewList();
		}
		this.counter=(this.counter+1)%this.list.length;
		return this.list[this.counter];
		
	}

	private Random            rndm=new Random();      //乱数
	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyNumListの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
