// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyRace.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
package SpeedDemon;

//タイトル候補
// Speed Freak
// Speed Merchant
// Speed Accross
// Speed Along
// Speed Past
// Speedster
// Speeder
// Speed Demon

// インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
import java.util.*;
import MyPackage.MyRecord;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public final class MyRace {
	private final String  RS_SCORE="tm.speedking.score";
	private final int     MAXCAR=5;

	private final int         CLEAR=-8, SEND=-10, SOFT1=-6, SOFT2=-7, SOFT3=-20, SOFT4=-21;
	
	private MyMissile missile=null;
	private MyCar     playerscar=null;
	private MyCar[]   rivalcars=new MyCar[MAXCAR];
	private MyRoad    road=null;
	private MyNumList numlist1=new MyNumList(3,3),
	                  numlist2=new MyNumList(6,3);
	private int       speed=10,minspeed=10,maxspeed=30,pluslevel=0,
	                  carwait=20,scorereset=0;
	private long      score=0L,level=1000L,bestscore=300L;
	private boolean   newrecord=false;
	
	private Font      largeFont=null,smallFont=null;
	
	//コンストラクタ
	public MyRace() {
		this.largeFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE);
		this.smallFont=Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL);
		
		this.missile=new MyMissile();
		this.playerscar=new MyCar();
		for (int i=0;i<this.MAXCAR;i++) {
			this.rivalcars[i]=new MyCar(this.playerscar);
			//this.rivalcars[i].setColor(i%3);
			//this.rivalcars[i].setPosition(i%3);
			//this.rivalcars[i].setSpeed(7+i);
			//this.rivalcars[i].startRunning(240);
		}
		this.road=new MyRoad(0,40);
		this.bestscore=MyRecord.loadLong(RS_SCORE,300L);
		this.newGame();
	}
	
	public void saveScore() {
		MyRecord.saveLong(RS_SCORE,this.bestscore);
	}
	
	//新規ゲーム
	private void newGame() {
		for (int i=0;i<this.MAXCAR;i++) {
			this.rivalcars[i].stopRunning();
			this.rivalcars[i].setVisible(false);
		}
		this.playerscar.setColor(0);
		this.playerscar.setPosition(1);
		this.playerscar.startRunning(50);
		this.score=0;
		this.speed=10;
		this.level=1000L;
		this.minspeed=10;
		this.carwait=-1;
		this.pluslevel=0;
		this.newrecord=false;
		this.missile.crash();
	}
	
	//メイン処理
	public void main(int keyEvent, int keyState) {
		int i,j;
		
		//キー判定
		if (this.playerscar.isCrashed()==false) {
			if (carwait>=0) { //ゲーム中
				if ((keyState&GameCanvas.UP_PRESSED)>0) {
					this.playerscar.movePosition(-1);
				} else if ((keyState&GameCanvas.DOWN_PRESSED)>0) {
					this.playerscar.movePosition(1);
				} else if ((keyState&GameCanvas.LEFT_PRESSED)>0) {
					if (this.speed>this.minspeed) this.speed--;
				} else if ((keyState&GameCanvas.RIGHT_PRESSED)>0) {
					if (this.speed<this.maxspeed) this.speed++;
				} else if ((keyState&GameCanvas.FIRE_PRESSED)>0) {
					if (this.missile.isRunning()==false) {
						this.missile.setPosition(this.playerscar.getY());
						this.missile.startRunning(70,this.speed+7);
					}
				}
				/*
				switch (keyEvent) {
				case Canvas.UP:
					this.playerscar.movePosition(-1);
					break;
				case Canvas.DOWN:
					this.playerscar.movePosition(1);
					break;
				case Canvas.LEFT:
					if (this.speed>this.minspeed) this.speed--;
					break;
				case Canvas.RIGHT:
					if (this.speed<this.maxspeed) this.speed++;
					break;
				case Canvas.FIRE:
					if (this.missile.isRunning()==false) {
						this.missile.setPosition(this.playerscar.getY());
						this.missile.startRunning(70,this.speed+7);
					}
					break;
				}
				*/
			} else { //スタート待機
				switch (keyEvent) {
				case CLEAR:
					if (this.scorereset>0) {
						this.scorereset=0;
					}
					break;
				case Canvas.FIRE:
					if (this.scorereset==11) {
						this.scorereset=0;
						this.bestscore=300L;
					} else {
						this.carwait=0;
						this.scorereset=0;
					}
					break;
				case Canvas.KEY_NUM1:
					if (this.scorereset<3) {
						this.scorereset++;
					} else {
						if ((this.scorereset>=6)&&(this.scorereset<9)) {
							this.scorereset++;
						} else if (this.scorereset==10) {
							this.scorereset++;
						}
					}
					break;
				case Canvas.KEY_NUM3:
					if ((this.scorereset>=3)&&(this.scorereset<6)) {
						this.scorereset++;
					} else if (this.scorereset==9) {
						this.scorereset++;
					}
					break;
				}
			}
		} else { //ゲームオーバー時
			if (keyEvent==Canvas.FIRE) {
				this.newGame();
			}
		}
		
		this.missile.move(this.speed);

		//プレイヤー車の移動等
		if (this.playerscar.isCrashed()) {
			this.playerscar.move(this.speed);
		} else {
			this.playerscar.move(0);
			if (this.carwait>=0) {
				this.score+=(long)this.speed;
				long sc=score/100L;
				if (sc>this.bestscore) {
					this.bestscore=sc;
					this.newrecord=true;
				}
			}
			if (this.score>this.level) {
				if (this.minspeed<this.maxspeed) {
					this.minspeed++;
					if (this.speed<this.minspeed) {
						this.speed=this.minspeed;
					}
					this.level+=100L*(long)this.speed;
				} else {
					if (this.pluslevel<6) {
						this.pluslevel++;
						this.level+=8000L+100L*(long)this.pluslevel;
					}
				}
			}
		}
		
		//障害物車の移動
		for (i=0;i<this.MAXCAR;i++) {
			this.rivalcars[i].move(this.speed);
		}
		
		//道路のスクロール
		this.road.scroll(this.speed);
		
		//障害物車カウント
		int carcount=0,idlecar=0;
		for (i=0;i<this.MAXCAR;i++) {
			if (this.rivalcars[i].isRunning()) {
				carcount++;
			} else {
				idlecar=i;
			}
		}
		
		//障害物車発生
		if ((carcount<this.MAXCAR)&&(this.carwait==0)) {
			this.rivalcars[idlecar].setColor(this.numlist1.getNext());
			this.rivalcars[idlecar].setPosition(this.numlist2.getNext());
			this.rivalcars[idlecar].setSpeed(7+this.rand(2+this.minspeed/10));
			this.rivalcars[idlecar].startRunning(240);
			//this.carwait=(168-this.speed*5)/4;
			this.carwait=(38-this.speed)/2-this.pluslevel/2;
		}
		if (this.carwait>0) this.carwait--;
		
		//衝突判定
		if (this.missile.isRunning()) {
			if (this.playerscar.collidesWith(this.missile)) {
				this.playerscar.crash();
				this.missile.crash();
				this.score-=100L*(long)(this.speed/2);
				this.newrecord=false;
				if (this.score<0L) this.score=0L;
			} else {
				for (i=0;i<this.MAXCAR;i++) {
					if (this.rivalcars[i].collidesWith(this.missile)) {
						this.missile.crash();
						this.rivalcars[i].crash();
						this.score-=100L*(long)(this.speed/2);
						this.newrecord=false;
						if (this.score<0L) this.score=0L;
						break;
					}
				}
			}
		}
		
		for (i=0;i<this.MAXCAR;i++) {
			//プレイヤー車との衝突
			if (this.playerscar.collidesWith(this.rivalcars[i])) {
				this.playerscar.crash();
				this.rivalcars[i].crash();
			}
			//障害物車同士の衝突
			for (j=i+1;j<this.MAXCAR;j++) {
				if (this.rivalcars[i].collidesWith(this.rivalcars[j])) {
					this.rivalcars[i].crash();
					this.rivalcars[j].crash();
				}
			}
		}
		
		
	}
	
	//ゲームの描写
	public void paint(Graphics g) {
		
		//背景
		g.setColor(0,128,0);
		g.fillRect(0,0,240,268);
		
		//道路
		this.road.paint(g);
		
		this.missile.paint(g);
		
		//各車
		this.playerscar.paint(g);
		for (int i=0;i<this.MAXCAR;i++) {
			this.rivalcars[i].paint(g);
		}
		
		//文字等
		g.setFont(this.largeFont);
		
		
		g.setColor(255,255,255);
		drawNukiMoji(g,"SPEED DEMON",120,0,0,0,0,Graphics.HCENTER|Graphics.TOP);
		//g.drawString("The Speed King",120,0,Graphics.HCENTER|Graphics.TOP);

		g.setColor(255,255,255);
		g.drawString("SPEED "+Integer.toString(this.speed),20,150,Graphics.LEFT|Graphics.TOP);
		g.drawString("SCORE "+Long.toString(this.score/100L),130,150,Graphics.LEFT|Graphics.TOP);
		if (this.newrecord) {
			g.setColor(this.rand(127)+128,
			           this.rand(127)+128,
			           this.rand(127)+128);
		}
		g.drawString("RECORD  "+Long.toString(this.bestscore),120,180,Graphics.HCENTER|Graphics.TOP);
		
		g.setColor(255,255,255);
		if (this.carwait<0) {
			g.drawString("PUSH FIRE TO START",120,235,Graphics.HCENTER|Graphics.TOP);
		}
		if (this.playerscar.isCrashed()) {
			g.drawString("GAME OVER",120,235,Graphics.HCENTER|Graphics.TOP);
		}
		
		if (this.scorereset>0) {
			g.setFont(smallFont);
			g.drawString(("ResetRecord").substring(0,this.scorereset),
			               120,255,Graphics.HCENTER|Graphics.TOP);
		}
		
	}


	private Random            rndm=new Random();      //乱数
	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 

	//中抜き文字の描写
	private void drawNukiMoji(Graphics g,String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyRaceの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
