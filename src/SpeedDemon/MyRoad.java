// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyRoad.java  クラス
//
// (C)Tomarky   200*.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
package SpeedDemon;

// インポート
//import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import javax.microedition.media.*;
//import java.io.*;
//import java.util.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
final class MyRoad {

	private MyBlock[] blocks=new MyBlock[10];
	private int       x=0,
	                  y=0,
	                  w=240,
	                  h=30;

	//コンストラクタ
	MyRoad(int x, int y) {
		this.x=x;
		this.y=y;
		for (int i=0;i<10;i++) {
			this.blocks[i]=new MyBlock((i%5)*60+20,(i/5)*30+70);
		}
	}
	
	public void scroll(int r) {
		for (int i=0;i<10;i++) {
			this.blocks[i].move(r);
		}
	}
	
	public void paint(Graphics g) {

		g.setColor(128,128,128);
		g.fillRect(this.x,this.y,this.w,this.h*3);

		g.setColor(255,255,255);
		g.fillRect(this.x,this.y            ,this.w,2);
		g.fillRect(this.x,this.y+this.h     ,this.w,2);
		g.fillRect(this.x,this.y+this.h*2   ,this.w,2);
		g.fillRect(this.x,this.y+this.h*3-2 ,this.w,2);

		for (int i=0;i<10;i++) {
			this.blocks[i].paint(g);
		}

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyRoadの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
