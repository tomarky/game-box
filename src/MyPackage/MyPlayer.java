// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// MyPlayer.java  音楽処理
//
// (C)Tomarky   2009.09.11-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// プログラムでMIDIとWAVの両方を使いたいなら２つインスタンス作れ
// さらにそれぞれの変数SoundTypeの値を適切に設定する必要がある
//
// closeメソッド(リソース開放)は必要に応じて実行してください
// closeメソッド実行後は他のどのメソッドも呼び出さないでください
// （エラーが生じプログラムが停止します）
//
// オープンアプリでは音の同時再生が４つまでと制限されているため以下のようにする
// ●使いたい音が４個以下なら
//    start,stop,prefetch,deallocate,setLoopCount
//   を使う
// ●使いたい音が５個以上なら
//   ・start,stop,prefetch,deallocate,change,setLoopCount     （音の切り替えは呼び出し側で管理する）
//   ・startEx,stopEx,prefetchEx(,deallocateEx),setLoopCountEx（音の切り替えはこのクラスで管理する）推奨
//   の２通りあり、どちらか一方で統一して使う
//
// このクラスの課題
//  ・ループに関する設定を可能にすること
//  ・CommandListenerに関すること
//  ・再生開始位置の指定等を可能にすること
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
package MyPackage;

// インポート
//import javax.microedition.lcdui.*;
//import javax.microedition.lcdui.game.*;
//import javax.microedition.midlet.*;
//import javax.microedition.rms.*;
//import java.util.*;
import javax.microedition.media.control.*;
import javax.microedition.media.*;
import java.io.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//
public class MyPlayer {
	public static final int  TYPE_WAV=0, TYPE_MIDI=1;

	// 変数宣言部
	private final String[]        CONTENTTYPE={"audio/x-wav","audio/midi"};
	private final int             LIMIT=4;
	private       int             SoundType=1; //ＷＡＶを使うなら 0、ＭＩＤＩなら 1
	
	private       boolean         success_load=true;
	
	private       int[]           iCur=new int[LIMIT]; //同時再生制限管理用
	private       boolean         noUse=false;
	private       int             PlayCount=0;
	private       Player[]        player;
	private       String[]        files;
	private       VolumeControl[] volume;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// コンストラクタ
/*
	public MyPlayer() {
		int i;
		for (i=0;i<LIMIT;i++)
			iCur[i]=-1;
		for (i=0;i<files.length;i++)
			load(i);
	}
*/
	public MyPlayer(String[] filepath, int type) {
		int i;
		int n=filepath.length;
		
		files=new String[n];
		player=new Player[n];
		volume=new VolumeControl[n];

		for (i=0;i<n;i++) {
			files[i]=new String(filepath[i]);
		}
		SoundType=Math.abs(type)%2;

		for (i=0;i<LIMIT;i++)
			iCur[i]=-1;
		for (i=0;i<files.length;i++)
			load(i);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// 音をロードしてリアライズ（準備状態）にする
	private void load(int index) {
		try {
			InputStream in;
			in=getClass().getResourceAsStream(files[index]);
			player[index]=Manager.createPlayer(in,CONTENTTYPE[SoundType]);
			player[index].realize();
			volume[index]=(VolumeControl)player[index].getControl("VolumeControl");
		} catch (Exception e) {
			System.out.println("(MyPlayer.load)::"+e.toString());
			success_load=false;
			noUse=true;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//適切なインデックスかどうか
	private boolean checkIndex(int index) {
		return ((index>=0)&&(index<player.length));
	}
	
	//適切な番号か
	private boolean checkNum(int num) {
		return ((num>=0)&&(num<LIMIT));
	}
	
	/*
	//その番号のプレイヤーが使われているかどうか
	private boolean existNum(int num) {
		return (iCur[num]>=0);
	}
	//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//ファイルに対応するプレイヤーの番号を返す
	public int getIndex(String filename) {
		for (int i=0;i<files.length;i++) {
			if (filename.equals(files[i]))
				return i;
		}
		return -1;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	// 音を使うか設定する
	public void setUse(boolean use) {
		if (success_load)
			noUse=!use;
	}
	// 音を使うかどうか
	public boolean getUse() {
		return !noUse;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//プレイヤーの状態を取得
	public int getState(int index) {
		if (checkIndex(index)==false) return Player.UNREALIZED;
		try {
			return player[index].getState();
		} catch (Exception e) {
			System.out.println("MyPlayer.getState::"+e.toString());
			return Player.UNREALIZED;
		}
	}
	public int getStateEx(int num) {
		if (checkNum(num)) {
			return getState(iCur[num]);
		}
		return Player.UNREALIZED;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	// 指定音を始めから再生する
	public boolean start(int index) {
		if (noUse) return false;
		if (checkIndex(index)==false) return false;
		
		try {
			if (player[index].getState()==Player.REALIZED) {
				if (PlayCount==LIMIT) return false;
				PlayCount++;
			}

			player[index].setMediaTime(0);
/*
		} catch (Exception e) {
			System.out.println("(MyPlayer.start)::"+e.toString());
			return false;
		}
		try {
*/
			player[index].start();
		} catch (Exception e) {
			System.out.println("(MyPlayer.start)::"+e.toString());
			return false;
		}
		return true;
	}
	public boolean startEx(int num) {
		if (checkNum(num)==false) return false;
		//if (existNum(num)==false) return false;
		return start(iCur[num]);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	// 指定音の再生を止める
	public boolean stop(int index) {
		if (checkIndex(index)==false) return false;
		try {
			player[index].stop();
		} catch (Exception e) {
			System.out.println("(MyPlayer.stop)::"+e.toString());
			return false;
		}
		return true;
	}
	public boolean stopEx(int num) {
		if (checkNum(num)==false) return false;
		//if (existNum(num)==false) return false;
		return stop(iCur[num]);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	// リアライズ（準備状態）に戻す
	public boolean deallocate(int index) {
		if (checkIndex(index)==false) return false;
		try {
			if (player[index].getState()!=Player.REALIZED)
				PlayCount--;
			else
				return false;

			player[index].deallocate();
		} catch (Exception e) {
			System.out.println("(MyPlayer.deallocate)::"+e.toString());
			return false;
		}
		return true;
	}
	/* 通常このメソッドを使うことは無い * /
	public boolean deallocateEx(int num) { 
		if (checkNum(num)==false) return false;
		//if (existNum(num)==false) return false;
		boolean b=deallocate(iCur[num]);
		iCur[num]=-1;
		return b;
	}
	/* -------------------------------- */

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	// プリフェッチ(待機状態）にする
	public boolean prefetch(int index) {
		if (checkIndex(index)==false) return false;
		
		try {
			if (player[index].getState()==Player.REALIZED) {
				if (PlayCount==LIMIT) return false;
				PlayCount++;
			}

			player[index].prefetch();
		} catch (Exception e) {
			System.out.println("(MyPlayer.prefetch)::"+e.toString());
			return false;
		}
		return true;
	}
	public boolean prefetchEx(int num, int index) {
		if (checkNum(num)==false) return false;
		if (iCur[num]==index) return false;
		//if (existNum(num))
			deallocate(iCur[num]);
		iCur[num]=index;
		return prefetch(index);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	// 音の切り替え処理（deallocateとprefetchを一度に済ます、利便性のためのメソッド）
	// 指定音を準備状態に戻し、新しい音を待機状態にする
	public boolean change(int deallocateIndex, int prefetchIndex) {
		//if (deallocateIndex>=0) 
			deallocate(deallocateIndex);
		return prefetch(prefetchIndex);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	// 全リソースを開放する
	//（このメソッド実行後はこのクラスのメソッドは使用できません）
	public void close() {
		int i;
		PlayCount=LIMIT;
		for (i=0;i<LIMIT;i++)
			iCur[i]=-1;
			
		for (i=0;i<files.length;i++) {
			try {
				player[i].close();
			} catch (Exception e) {
				System.out.println("MyPlayer.close("+i+":):"+e.toString());
			}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ループ回数の設定 (STARTEDのときに実行してはならない）
	public boolean setLoopCount(int index, int count) {
		if (checkIndex(index)==false) return false;
		if ((count==0)||(count<-1)) return false;
		try {
			if (player[index].getState()==Player.STARTED) return false;
		
			player[index].setLoopCount(count);
		} catch (Exception e) {
			System.out.println("(MyPlayer.setLoopCount)::"+e.toString());
			return false;
		}
		return true;
	}
	public boolean setLoopCountEx(int num, int count) {
		if (checkNum(num)==false) return false;
		//if (existNum(Num)==false) return false;
		return setLoopCount(iCur[num],count);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	//ボリュームを設定する
	public boolean setLevel(int index, int level) {
		if (checkIndex(index)==false) return false;
		volume[index].setLevel(level);
		return true;
	}
	public boolean setLevelEx(int num, int level) {
		if (checkNum(num)==false) return false;
		return setLevel(iCur[num],level);
	}
	public void setLevelAll(int level) {
		for (int i=0;i<volume.length;i++)
			volume[i].setLevel(level);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//ボリュームを取得する
	public int getLevel(int index) {
		if (checkIndex(index)==false) return -1;
		return volume[index].getLevel();
	}
	public int getLevelEx(int num) {
		if (checkNum(num)==false) return -1;
		return getLevel(iCur[num]);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //MyPlayerの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
