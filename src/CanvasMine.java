// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// CanvasMine.java メイン処理部
//
// (C)Tomarky   2009.07.13-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;
//import javax.microedition.rms.*;   //データセーブ用
//import java.io.*;                  //バイト変換用
//import javax.microedition.media.*; //音楽再生
import MyPackage.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
class CanvasMine extends Canvas1 implements CommandListener {
	private final String RN_BESTSCORE="tm.mine.bestscore";
	private final String GAME_VERSION="1.2";

	private Graphics    g;
	private Image[]     imgs=new Image[3];
	private int         keyEvent=-999;         //キーイベント
	private int         keyState;              //キー状態
	private boolean     bl=true;
	private boolean     cf=false;
	private Command[]   cmds=new Command[6];//コマンド
	private int         cmd=-1;
	private Random      rndm=new Random();      //乱数

	private int         w=240;
	private int         h=268;
	//private MIDlet      midlet;
	
	private TiledLayer  tl;

	private int         cx=0;
	private int         cy=0;
	
	private int[][]     minemap=new int[15][15];
	private int[][]     openmap=new int[15][15];
	private int         openCount=0;
	private int         flagCount=0;
	private int         timeCount=0;
	private int         bestScore=999;

	private boolean     getBest=false;
	private boolean     gameComplete=false;
	private boolean     flagGameOver=false;
	private boolean     firstClick=true;
	private boolean     flag1=false;
	
	private boolean     flag2=false;	
	private int         f2cnt=0;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasMine() {
		//キーイベントの抑制
		super(false);
		
		try {
		tl=new TiledLayer(15,15,Image.createImage("/mine.png"),10,10);
		} catch (Exception e) {}
		
		tl.setPosition(45,45);
		
		/*
		//MIDlet操作用
		midlet=m;
		*/
		bestScore=MyRecord.loadInt(RN_BESTSCORE, 999);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//実行
	public void run() {
		long sleepTime=0L;

		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		cmds[0]=new Command("はい",  Command.OK,    0);
		cmds[1]=new Command("いいえ",Command.CANCEL,0);
		cmds[2]=new Command("フラグ",Command.SCREEN,0);
		cmds[3]=new Command("戻る",  Command.BACK,  0);
		cmds[4]=new Command("終了",  Command.EXIT,  0);
		cmds[5]=new Command("中断",  Command.STOP,  0);

		addCommand(cmds[4]);
		addCommand(cmds[2]);
		
		//コマンドリスナーの指定
		setCommandListener(this);

		newgame();

		while (bl) {
		
			//コマンドイベントの処理
			switch (cmd) {
			case 4:
				flag1=true;
				removeCommand(cmds[2]);
				removeCommand(cmds[4]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				break;
			
			case 0:
				if (flag1) bl=false;
				if (flag2) {
					bestScore=999;
					MyRecord.saveInt(RN_BESTSCORE,bestScore);
					flag2=false;
					removeCommand(cmds[0]);
					removeCommand(cmds[1]);
					addCommand(cmds[4]);
					addCommand(cmds[2]);
					keyEvent=-999;
				}
				break;
			
			case 1:
				if (flag1) flag1=false;
				if (flag2) flag2=false;
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				addCommand(cmds[4]);
				addCommand(cmds[2]);
				keyEvent=-999;
				break;
			
			case 2:
				marker(cx,cy);
				break;
			}
			cmd=-1;

			//キーイベントの処理
			if ((flag1)||(flag2)) keyEvent=-999;
			switch (keyEvent) {
			case UP:
				if (cy>0) cy--;
				break;
				
			case DOWN:
				if (cy<14) cy++;
				break;
				
			case LEFT:
				if (cx>0) cx--;
				break;
				
			case RIGHT:
				if (cx<14) cx++;
				break;
				
			case FIRE:
				if (flagGameOver) 
					newgame();
				else 
					clickCell(cx,cy);
				break;
			}
			keyEvent=-999;
			
			//ゲームクリア処理
			if ((flagGameOver==false)&&(openCount==195)) {
				flagGameOver=true;
				gameComplete=true;
				for (int i=0;i<15;i++) 
					for (int j=0;j<15;j++)
						if (openmap[j][i]<2) tl.setCell(j,i,2);
				flagCount=30;
				if ((timeCount/10)<bestScore) {
					getBest=true;
					bestScore=timeCount/10;
					MyRecord.saveInt(RN_BESTSCORE,bestScore);
				}
			}
			
			
			//描画
			g.setColor(192,192,192);
			g.fillRect(0,0,w,h);
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.setColor(0,0,0);
			g.drawString("マインスイーパ",0,0,Graphics.LEFT|Graphics.TOP);
			g.drawString("Best: "+bestScore+" 秒",120,0,Graphics.LEFT|Graphics.TOP);
			g.drawString("mine: "+(30-flagCount),0,17,Graphics.LEFT|Graphics.TOP);
			g.drawString("time: "+(timeCount/10)+" 秒",120,17,Graphics.LEFT|Graphics.TOP);
			g.setColor(128,128,128);
			g.drawLine(44,44,44+150,44);
			g.drawLine(44,44,44,44+150);
			tl.paint(g);
			g.setColor(255,0,0);
			g.drawRect(44+10*cx,44+10*cy,10,10);
			if (flagGameOver) {
				g.setColor(0,0,0);
				if (gameComplete) {
					g.drawString("ゲームクリアー",120,200,Graphics.HCENTER|Graphics.TOP);
					if (getBest) {
						int c1,c2,c3;
						c1=rand(256); c2=rand(256); c3=rand(256);
						g.setColor(c1,c2,c3);
						g.drawString("ベストタイムです！("+bestScore+"秒)",120,220,Graphics.HCENTER|Graphics.TOP);
					}
				} else {
					g.drawString("ゲームオーバー",120,200,Graphics.HCENTER|Graphics.TOP);
				}
				g.setColor(0,0,255);
				g.drawString("Press（●）Key To NewGame",120,240,Graphics.HCENTER|Graphics.TOP);
			}
			if (flag1) drawComment("終了しますか？");
			if (flag2) drawComment("Bestﾀｲﾑをﾘｾｯﾄしますか?");
			flushGraphics();
            
            //タイムカウント
			if ((flagGameOver==false)&&(firstClick==false)&&(flag1==false)&&(flag2==false)&&(timeCount<9990)) {
				timeCount++;
			}
			
			//スリープ
			while (System.currentTimeMillis()<sleepTime+100);// &&
			sleepTime=System.currentTimeMillis();
		}

		//終了
		if (cf==false) Project017.myproject.finishGame();
		//if (cf==false) midlet.notifyDestroyed();

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		switch (keyCode) {
		case KEY_NUM1: 
			if (f2cnt==0) f2cnt++; 
			else if (f2cnt==9) {
				if ((flag1==false)&&(flag2==false)) {
					f2cnt=0;
					flag2=true;
					removeCommand(cmds[4]);
					removeCommand(cmds[2]);
					addCommand(cmds[0]);
					addCommand(cmds[1]);
				}
			}
			break;
		case KEY_NUM2: if (f2cnt==1) f2cnt++; break;
		case KEY_NUM3: if (f2cnt==2) f2cnt++; break;
		case KEY_NUM4: if (f2cnt==3) f2cnt++; break;
		case KEY_NUM5: if (f2cnt==4) f2cnt++; break;
		case KEY_NUM6: if (f2cnt==5) f2cnt++; break;
		case KEY_NUM7: if (f2cnt==6) f2cnt++; break;
		case KEY_NUM8: if (f2cnt==7) f2cnt++; break;
		case KEY_NUM9: if (f2cnt==8) f2cnt++; break;
		}
		
		keyEvent=getGameAction(keyCode);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	public void keyRepeated(int keyCode) {
		switch (getGameAction(keyCode)) {
		case UP:
		case DOWN:
		case LEFT:
		case RIGHT:
			keyEvent=getGameAction(keyCode);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		if (c==cmds[0]) cmd=0;
		if (c==cmds[1]) cmd=1;
		if (c==cmds[2]) cmd=2;
		if (c==cmds[3]) cmd=3;
		if (c==cmds[4]) cmd=4;
		if (c==cmds[5]) cmd=5;
		repaint();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,h-20,w,20);
		g.setColor(0,128,255);
		g.drawRect(0,h-20,w,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,w/2,h-18,Graphics.HCENTER|Graphics.TOP);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void newgame() {
		int i,j,k,n,x,y;

		openCount=0;
		flagCount=0;
		timeCount=0;
		firstClick=true;
		flagGameOver=false;
		getBest=false;
		gameComplete=false;

		//Init
		for (i=0;i<15;i++) 
			for (j=0;j<15;j++) {
				minemap[j][i]=0;
				openmap[j][i]=0;
			}

		//Set Mine
		for (i=0;i<2;i++)
			for (j=0;j<15;j++)
				minemap[j][i]=-1;

		//Shuffle
		for (k=0;k<30;k++)
			for (i=0;i<15;i++) 
				for (j=0;j<15;j++) {
					x=rand(15);
					y=rand(15);
					n=minemap[x][y];
					minemap[x][y]=minemap[j][i];
					minemap[j][i]=n;
				}
		
		countMine();
		
		tl.fillCells(0,0,15,15,1);
		
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void countMine() {
		int i,j;
		
		//カウント1
		for (i=0;i<15;i++)
			for (j=0;j<14;j++) {
				//右
				if ((minemap[j][i]>=0)&&(minemap[j+1][i]<0)) minemap[j][i]++;
				//左
				if ((minemap[j][i]<0)&&(minemap[j+1][i]>=0)) minemap[j+1][i]++;
				//下
				if ((minemap[i][j]>=0)&&(minemap[i][j+1]<0)) minemap[i][j]++;
				//上
				if ((minemap[i][j]<0)&&(minemap[i][j+1]>=0)) minemap[i][j+1]++;
			}
		
		//カウント2
		for (i=0;i<14;i++)
			for (j=0;j<14;j++) {
				//右下
				if ((minemap[j][i]>=0)&&(minemap[j+1][i+1]<0)) minemap[j][i]++;
				//右上
				if ((minemap[j][i+1]>=0)&&(minemap[j+1][i]<0)) minemap[j][i+1]++;
				//左下
				if ((minemap[j+1][i]>=0)&&(minemap[j][i+1]<0)) minemap[j+1][i]++;
				//左上
				if ((minemap[j+1][i+1]>=0)&&(minemap[j][i]<0)) minemap[j+1][i+1]++;
			}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void firstCheck(int x, int y) {
		if (minemap[x][y]==0) return;
		
		System.out.println("Check!");
		
		int i,j,x2,y2;
		
		for (i=y-1;i<=y+1;i++)
			for (j=x-1;j<=x+1;j++) {
				if ((j>=0)&&(j<=14)&&(i>=0)&&(i<=14))
					if (minemap[j][i]<0) {
						do {
							x2=rand(15);
							y2=rand(15);
						} while (((Math.abs(x2-x)<2)&&(Math.abs(y2-y)<2))||(minemap[x2][y2]<0));
						minemap[x2][y2]=-1;
						minemap[j][i]=0;
					}
			}
		
		for (i=0;i<15;i++)
			for (j=0;j<15;j++)
				if (minemap[j][i]>0) minemap[j][i]=0;
		
		countMine();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void clickCell(int x, int y) {
		if ((x<0)||(x>14)||(y<0)||(y>14)) return;
		if (openmap[x][y]>1) return;
		
		if (firstClick) {
			firstClick=false;
			firstCheck(x,y);
		}
		
		if (minemap[x][y]==0) {
			openmap[x][y]=3;
			tl.setCell(x,y,7);
			openCount++;
			clickCell(x+1,y);
			clickCell(x-1,y);
			clickCell(x,y+1);
			clickCell(x,y-1);
			clickCell(x+1,y+1);
			clickCell(x-1,y+1);
			clickCell(x+1,y-1);
			clickCell(x-1,y-1);
		}
		else if (minemap[x][y]>0) {
			openmap[x][y]=3;
			tl.setCell(x,y,7+minemap[x][y]);
			openCount++;
		}
		else {
			flagGameOver=true;
			for (int i=0;i<15;i++)
				for (int j=0;j<15;j++) {
					if (openmap[j][i]<2) {
						if (minemap[j][i]==0) tl.setCell(j,i,7);
						if (minemap[j][i]>0)  tl.setCell(j,i,7+minemap[j][i]);
						if (minemap[j][i]<0)  tl.setCell(j,i,6);
					}
					else if (openmap[j][i]==2) {
						if (minemap[j][i]>=0) tl.setCell(j,i,5);
					}
					openmap[j][i]=3;
				}
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void marker(int x, int y) {
		if (flagGameOver) return;
	
		switch (openmap[x][y]) {
		case 0:
			openmap[x][y]=2;
			tl.setCell(x,y,2);
			flagCount++;
			break;
			
		case 1:
			openmap[x][y]=0;
			tl.setCell(x,y,1);
			break;
			
		case 2:
			openmap[x][y]=1;
			tl.setCell(x,y,3);
			flagCount--;
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		bl=false;
		cf=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //CanvasMineの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
