// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// CanvasSnake.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;
//import javax.microedition.rms.*;   //データセーブ用
//import java.io.*;                  //バイト変換用
//import javax.microedition.media.*; //音楽再生
import MyPackage.*;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
class CanvasSnake extends Canvas1 implements CommandListener {
	private final String[]  RS_NAME={"tm.snk.smp","tm.snk.blk","tm.snk.run","tm.snk.lmt"};
	private final String    GAME_VERSION="1.1";
	
	private Graphics    g;
	private Image[]     imgs=new Image[3];
	private int         keyEvent=-999;         //キーイベント
	private int         keyState;              //キー状態
	private boolean     bl=true;
	private boolean     cf=false;
	private Command[]   cmds=new Command[6];//コマンド
	private int         cmd=-1;
	private Random      rndm=new Random();      //乱数

	private int         w=240;
	private int         h=268;
	//private MIDlet      midlet;
	private String      gmver;         

	private int[][]     fld=new int[22][22];
	private int         cx,cy,vx,vy,tx=0,ty=0,tc=0;
	private final int   psize=60;
	private int[]       px=new int[60];
	private int[]       py=new int[60];
	private int         pt,tg,fg,hd;
	private final int   GS_PLAY=0, GS_GAMEOVER=1, GS_IDLE=2;
	private int         gamestate=GS_IDLE;
	private int         score;
	private final int   GM_SIMPLE=0, GM_BLOCK=1, GM_MOVE=2, GM_LIMIT=3;
	private int         gamemode=0;
	private final int   gmcount=4;
	private String[]    menu1={"Simple","Block","Move","Limit"};
	private int[]       bestscore={0,0,0,0};
	private boolean     getbest;
	private boolean     endflag=false, resetflag=false;
	private int         lmtcount,lmtbase,lmtscore;

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasSnake() {
		//キーイベントの抑制
		super(false);
		
		
		//MIDlet操作用
		//midlet=m;
		
		//gmver=m.getAppProperty("MIDlet-Version");
		gmver=GAME_VERSION;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//実行
	public void run() {
		int  i,j,x,y,rx=0,ry=0;
		long sleepTime=0L;

		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		cmds[0]=new Command("はい",  Command.OK,    0);
		cmds[1]=new Command("いいえ",Command.CANCEL,0);
		cmds[2]=new Command("ﾘｾｯﾄ",  Command.SCREEN,0);
		cmds[3]=new Command("戻る",  Command.BACK,  0);
		cmds[4]=new Command("終了",  Command.EXIT,  0);
		cmds[5]=new Command("中断",  Command.STOP,  0);

		addCommand(cmds[4]);
		addCommand(cmds[2]);
		

		//コマンドリスナーの指定
		setCommandListener(this);
		
		loadScore();
		
		newgame();

		while (bl) {
		
			//コマンドイベントの処理
			switch (cmd){
			case 0:
				if (endflag) bl=false;
				if (resetflag) {
					for(i=0;i<gmcount;i++) {
						bestscore[i]=0;
					}
					removeCommand(cmds[0]);
					removeCommand(cmds[1]);
					addCommand(cmds[4]);
					addCommand(cmds[2]);
					resetflag=false;
					keyEvent=-999;
				}
				break;
				
			case 1:
				removeCommand(cmds[0]);
				removeCommand(cmds[1]);
				addCommand(cmds[4]);
				if (gamestate==GS_IDLE) {
					addCommand(cmds[2]);
				}
				resetflag=false;
				endflag=false;
				keyEvent=-999;
				break;
			case 2:
				removeCommand(cmds[4]);
				removeCommand(cmds[2]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				resetflag=true;
				break;

			case 4:
				removeCommand(cmds[4]);
				removeCommand(cmds[2]);
				addCommand(cmds[0]);
				addCommand(cmds[1]);
				endflag=true;
				break;
			}
			cmd=-999;
			
			if ((endflag)||(resetflag)) {
				keyEvent=-999;
			} else {
			
				switch (gamestate) {
				case GS_PLAY:
			
					//キーイベントの処理
					switch (keyEvent) {
					case LEFT:
						if ((hd!=2)||(pt==0)) { 
							vx=-1; vy=0; hd=1; }
						break;
					case RIGHT:
						if ((hd!=1)||(pt==0)) {
							vx=1; vy=0; hd=2; }
						break;
					case UP:
						if ((hd!=4)||(pt==0)) {
							vx=0; vy=-1; hd=3; }
						break;
					case DOWN:
						if ((hd!=3)||(pt==0)) {
							vx=0; vy=1; hd=4; }
						break;
					}
					keyEvent=-999;
					
					//餌（ターゲット）の発生
					if (tg==0) {
						rx=0; ry=0;
						while (fld[rx][ry]>0) {
							rx=rand(21);
							ry=rand(21);
						}
						tg=1;
						fld[rx][ry]=3;
						switch (gamemode) {
						case GM_LIMIT:
							lmtcount=100;
							lmtbase=Math.abs(cx-rx)+Math.abs(cy-ry);
							break;
						case GM_MOVE:
							int mmm=rand(4);
							switch (mmm){
							case 0: tx=0; ty=1; break;
							case 1: tx=0; ty=-1; break;
							case 2: tx=1; ty=0; break;
							case 3: tx=-1; ty=0; break;
							default: tx=0; ty=0; break;
							}
							break;
						}
					}

					//尾の末端を消す
					if (fg==0) {
						fld[px[pt]][py[pt]]=0;
					} else {
						fg=0;
					}
					
					//胴体の情報をずらす
					for (i=pt;i>0;i--) {
						px[i]=px[i-1];
						py[i]=py[i-1];
					}
					
					//蛇の移動
					if ((cx>0)&&(cx<21)) cx+=vx;
					if ((cy>0)&&(cy<21)) cy+=vy;
					
					//蛇の移動位置に対する処理
					switch (fld[cx][cy]) {
					case 1: //壁
					case 2: //胴体
						fld[cx][cy]=4;
						if (bestscore[gamemode]<score) {
							getbest=true;
							bestscore[gamemode]=score;
						}
						gamestate=GS_GAMEOVER;
						break;
					case 3: //餌
						if (pt<psize-1) { //伸びる
							pt++;
							fg=1;
						}
						tg=0;
						if (gamemode==GM_LIMIT) {
							lmtscore+=lmtcount+lmtbase;
							score=lmtscore/10;
						} else {
							score+=10;
						}
					default:
						fld[cx][cy]=2;
					}
					px[0]=cx; py[0]=cy;
					
					//リミットモード
					if ((gamemode==GM_LIMIT)&&(tg==1)) {
						if ((vx!=0)||(vy!=0)) lmtcount--;
						if (lmtcount==0) {
							fld[cx][cy]=4;
							if (bestscore[gamemode]<score) {
								getbest=true;
								bestscore[gamemode]=score;
							}
							gamestate=GS_GAMEOVER;
						}
					}

					
					//ムーブモード　餌の移動
					if ((tg==1)&&(gamemode==GM_MOVE)) {
						if (tc==0) {
							int mmm2=rand(1000);
							if (mmm2>900) {
								int mmm3=rand(4);
								switch (mmm3){
								case 0: tx=0; ty=1; break;
								case 1: tx=0; ty=-1; break;
								case 2: tx=1; ty=0; break;
								case 3: tx=-1; ty=0; break;
								default: tx=0; ty=0; break;
								}
							}
							if (fld[rx+tx][ry]>0) {
								tx=-tx;
							}
							if (fld[rx][ry+ty]>0) {
								ty=-ty;
							}
							if (fld[rx+tx][ry+ty]==0) {
								fld[rx][ry]=0;
								rx+=tx; ry+=ty;
								fld[rx][ry]=3;
							}
							tc=1;
						} else {
							tc--;
						}
							
					}
					
					break;
	
				case GS_GAMEOVER: //ゲームオーバー
					switch (keyEvent) {
					case FIRE:
						addCommand(cmds[2]);
						gamestate=GS_IDLE;
						break;
					}
					keyEvent=-999;
					break;
	
				case GS_IDLE: //ゲームアイドル
					switch (keyEvent) {
					case LEFT:
					case UP:
						gamemode=(gamemode+gmcount-1)%gmcount;
						break;
					case RIGHT:
					case DOWN:
						gamemode=(gamemode+1)%gmcount;
						break;
					case FIRE:
						newgame();
						removeCommand(cmds[2]);
						gamestate=GS_PLAY;
						break;
					}
					keyEvent=-999;
					break;
				}
			
			}


			//描画
			g.setColor(255,255,255);
			g.fillRect(0,0,getWidth(),getHeight());
			g.setColor(0,0,0);
			g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
			g.drawString("Snake "+gmver,0,0,Graphics.LEFT|Graphics.TOP);
			g.drawString("Score "+score,0,20,Graphics.LEFT|Graphics.TOP);
			
			//フィールドの描写
			for (i=0;i<22;i++) {
				y=i*10+40;
				for (j=0;j<22;j++) {
					x=j*10+10;
					switch (fld[j][i]) {
					case 1:
						g.setColor(0,0,0);
						break;
					case 2:
						g.setColor(0,0,255);
						break;
					case 3:
						g.setColor(0,255,255);
						break;
					case 4:
						g.setColor(255,0,0);
						break;
					}
					if (fld[j][i]>0)
						g.fillRect(x,y,9,9);
				}
			}
			
			switch (gamestate) {

			case GS_GAMEOVER:
				if (getbest) {
					int c1,c2,c3;
					c1=rand(256); c2=rand(256); c3=rand(256);
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
					g.setColor(255,255,255);
					drawNukiMoji("Best Record!!",w/2,h/2-20,c1,c2,c3,Graphics.HCENTER|Graphics.TOP);
					g.setColor(255,255,255);
					drawNukiMoji(""+score,w/2,h/2+1,c1,c2,c3,Graphics.HCENTER|Graphics.TOP);
				}
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_LARGE));
				g.setColor(0,0,0);
				drawNukiMoji("Game Over!",w/2,h-70,255,0,0,Graphics.HCENTER|Graphics.TOP);
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
				g.setColor(255,255,255);
				drawNukiMoji("Push（●）Key",w/2,h-45,0,255,0,Graphics.HCENTER|Graphics.TOP);

			case GS_PLAY:
				g.setColor(0,0,0);
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
				g.drawString(menu1[gamemode],w/2,0,Graphics.LEFT|Graphics.TOP);
				g.drawString(""+bestscore[gamemode],w-1,0,Graphics.RIGHT|Graphics.TOP);
				if (gamemode==GM_LIMIT) {
					g.drawString("Count "+lmtcount,w/2,20,Graphics.LEFT|Graphics.TOP);
				}
				break;

			case GS_IDLE:
				for (i=0;i<gmcount;i++) {
					y=i*20+60;
					if (i==gamemode) g.setColor(255,255,0);
					else             g.setColor(128,128,128);
					g.fillRect(w/5,y,(w*3)/5,19);
					g.setColor(0,0,0);
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
					g.drawString(menu1[i],w/5+2,y+1,Graphics.LEFT|Graphics.TOP);
					g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_SMALL));
					g.drawString(""+bestscore[i],(w*4)/5-2,y+5,Graphics.RIGHT|Graphics.TOP);
				}
				g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
				g.setColor(0,0,0);
				drawNukiMoji("Push（●）Key to Start",w/2,h-45,255,255,255,Graphics.HCENTER|Graphics.TOP);
				break;
			}

			if (endflag) drawComment("終了しますか？");
			if (resetflag) drawComment("ﾍﾞｽﾄｽｺｱをﾘｾｯﾄしますか？");

			flushGraphics();
            
			//スリープ
			while (System.currentTimeMillis()-sleepTime<100L);
			sleepTime=System.currentTimeMillis();
		}

		//saveScore();
		
		//終了
		if (cf==false) {
			saveScore();
			Project017.myproject.finishGame();
		}
		//if (cf==false) midlet.notifyDestroyed();

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		
		keyEvent=getGameAction(keyCode);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		if (c==cmds[0]) cmd=0;
		if (c==cmds[1]) cmd=1;
		if (c==cmds[2]) cmd=2;
		if (c==cmds[3]) cmd=3;
		if (c==cmds[4]) cmd=4;
		if (c==cmds[5]) cmd=5;
		repaint();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,h-20,w,20);
		g.setColor(0,128,255);
		g.drawRect(0,h-20,w,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,w/2,h-18,Graphics.HCENTER|Graphics.TOP);
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		bl=false;
		cf=true;
		saveScore();
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void newgame() {
		int i,j,k,rx,ry;

		for (i=0;i<22;i++)
			for (j=0;j<22;j++)
				fld[j][i]=0;
		for (i=0;i<22;i++) {
			fld[i][0]=1;
			fld[i][21]=1;
			fld[0][i]=1;
			fld[21][i]=1;
		}
		
		if (gamemode==GM_BLOCK) {
			for (i=0;i<2;i++)
				for (j=0;j<2;j++)
					for (k=0;k<2;k++) {
						rx=0; ry=0;
						while (fld[rx][ry]>0) {
							rx=rand(8)+2+j*10;
							ry=rand(8)+2+i*10;
						}
						fld[rx][ry]=1;
					}
		}
		
		

		vx=0; vy=0;
		cx=10; cy=10;
		pt=0;
		px[0]=cx; py[0]=cy;
		
		fld[cx][cy]=2;
		
		tg=0; fg=0; hd=0;
		
		score=0;
		getbest=false;
		lmtcount=0;
		lmtbase=0;
		lmtscore=0;

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
	
	private void saveScore() {
		for (int i=0;i<4;i++) {
			MyRecord.saveInt(RS_NAME[i],bestscore[i]);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	private void loadScore() {
		for (int i=0;i<4;i++) {
			bestscore[i]=MyRecord.loadInt(RS_NAME[i],0);
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //CanvasSnakeの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
