// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
// CanvasSpeedDmn.java メイン処理部
//
// (C)Tomarky   2009.01.01-
// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//インポート
import javax.microedition.lcdui.game.*;
import javax.microedition.lcdui.*;
import javax.microedition.midlet.*;
import java.util.*;                  //乱数や時間
//import java.io.*;                  //InputStream OutputStream
//import javax.microedition.rms.*;   //データセーブ用
//import javax.microedition.media.*; //音楽再生
//import javax.microedition.media.control.*; //トーン・ボリューム
//import javax.microedition.io;      //通信


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

//メイン処理(キャンバス)
public class CanvasSpeedDmn extends Canvas1 implements CommandListener {
	private final int         DISPLAY_WIDTH  = 240,
	                          DISPLAY_HEIGHT = 268;
	private final int         CLEAR=-8, SEND=-10, SOFT1=-6, SOFT2=-7, SOFT3=-20, SOFT4=-21;
	private final long        SLEEPTIME = 100L;
//	private MIDlet            midlet;
	private Graphics          g;
	private boolean           RunningFlag=true;
	private boolean           CallFlag=false;
	private int               keyEvent=-999;           //キーイベント
//	private int               keyPsEvent=-999;         //キープレスイベント
//	private int               keyRpEvent=-999;         //キーリピートイベント
//	private int               keyRsEvent=-999;         //キーリリースイベント
//	private int               keyState;              //キー状態
	private Command[]         commands=new Command[3];//コマンド
	private int               cmdnum=-1;
//	private Image[]           images=new Image[3];


// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コンストラクタ
	CanvasSpeedDmn() {
		//キーイベントの抑制
		super(false);
		
		//MIDlet操作用
		//midlet=m;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//実行
	public void run() {
		long sleepTime=0L;

		//グラフィックスの取得
		g=getGraphics();

		//コマンドの生成
		commands[0]=new Command("はい",  Command.OK,    0);
		commands[1]=new Command("いいえ",Command.CANCEL,0);
		commands[2]=new Command("終了",  Command.SCREEN,0);

		addCommand(commands[2]);
		
		//コマンドリスナーの指定
		setCommandListener(this);

		SpeedDemon.MyRace myrace=new SpeedDemon.MyRace();

		while (RunningFlag) {
		
			//コマンドイベントの処理
			switch (cmdnum) {
			case 2:
				RunningFlag=false;
				break;
			}
			cmdnum=-999;
			
			myrace.main(keyEvent,getKeyStates());
			keyEvent=-999;
		
			//描画
			
			myrace.paint(g);
			
			flushGraphics();
            
			//スリープ
			while (System.currentTimeMillis()<sleepTime+SLEEPTIME);
			sleepTime=System.currentTimeMillis();
		}
		
		myrace.saveScore();

		//終了
		if (CallFlag==false) {
			Project017.myproject.finishGame();
			 //midlet.notifyDestroyed();
		}

	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//キープレスイベント
	public void keyPressed(int keyCode) {
		if (keyCode==0) return;
		switch (keyCode) {
		case KEY_NUM1:
		case KEY_NUM3:
		case CLEAR:
		case SEND:
			keyEvent=keyCode;
			break;
		default:
			keyEvent=getGameAction(keyCode);
			break;
		}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//キーリピートイベント
	public void keyRepeated(int keyCode) {
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//キーリリースイベント
	public void keyReleased(int keyCode) {
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//コマンドイベント
	public void commandAction(Command c,Displayable disp) {
		int i;
		for (i=0;i<commands.length;i++)
			if (c==commands[i]) {
				cmdnum=i;
				break;
			}
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//コメント描写
	public void drawComment(String s) {
		g.setColor(255,255,255);
		g.fillRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,128,255);
		g.drawRect(0,DISPLAY_HEIGHT-20,DISPLAY_WIDTH,20);
		g.setColor(0,0,0);
		g.setFont(Font.getFont(Font.FACE_SYSTEM,Font.STYLE_PLAIN,Font.SIZE_MEDIUM));
		g.drawString(s,DISPLAY_WIDTH/2,DISPLAY_HEIGHT-18,Graphics.HCENTER|Graphics.TOP);
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	//中抜き文字の描写
	private void drawNukiMoji(String s, int x, int y, int rIn, int gIn, int bIn, int anchor) {
		int i,j;
		
		for (i=-1;i<2;i++)
			for (j=-1;j<2;j++)
				g.drawString(s,x+j,y+i,anchor);
		g.setColor(rIn,gIn,bIn);
		g.drawString(s,x,y,anchor);
	}
	private void drawNukiMoji(String s, int x, int y, int rIn,   int gIn,  int bIn,
	                                                  int rOut,  int gOut, int bOut, int anchor) {
		g.setColor(rOut,gOut,bOut);
		drawNukiMoji(s,x,y,rIn,gIn,bIn,anchor);
	}
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

/*
	private Random            rndm=new Random();      //乱数
	//乱数の計算
	private int rand(int num) {
		return (rndm.nextInt()>>>1)%num;
	} 
//*/

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

	//終了命令
	public void callFinish() {
		RunningFlag=false;
		CallFlag=true;
	}

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //

} //CanvasSpeedDmnの終わり

// @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@ //
