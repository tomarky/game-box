オープンアプリ『ゲームＢＯＸ』

いくつかのミニゲームのセットです

・もぐらたたき
・マインスイーパ
・スネーク
・落ちゲー１
・落ちゲー２
・麻雀牌パズル
・避けゲー


アプリ配布リンク: http://tomarky.html.xdomain.jp/mobile/oap/p017.hdml


※オープンアプリとはau携帯電話(ガラケー)で動かすことができるアプリのことです
　(参考リンク http://www.au.kddi.com/ezfactory/tec/spec/openappli.html ）