
★Project017.javaの編集について
GAMECOUNTの値を＋１
gameTitle,gameIcon,newGameCanvasを適切に拡張

★メインクラスについて
Canvas1 extends GameCanvas implements Runnable
を
CanvasGameTitle extends Canvas1
に変更

★コンストラクタについて
クラス名の変更にあわせて変更する
MIDletの情報が必要ないなら引数から削除しておくのが望ましい

★MyRecord,MyPlayerについて
MyPackageを利用してない場合は
import MyPackage.*;
を追記する	

★バージョン表示について
MIDletヴァージョンからアプリのバージョンに変更
（つまりMIDletからヴァージョン情報を得るのではなく
 あらかじめ記述しておく）
appver="var "+m.getAppProperty("MIDlet-Version");
を
appver="var ?.?";
に変更する

★終了処理について
if (cf==false) midlet.notifyDestroyed();
  又は、if (CallFlag==false) midlet.notifyDestroyed();
を
if (cf==false) Project017.myproject.finishGame();
  又は、if (CallFlag==false) Project017.myproject.finishGame();
に変更する

★リソースについて
リソースファイル名は固有の名前を割り当てるようにする
他のアプリと共有するようにアプリを作ることが望ましい
ファイル名の変更にあわせてプログラムソース内での名前を変更するのを忘れずに！

★内容について
容量の関係など場合によっては削減するとよい


